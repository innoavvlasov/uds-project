# uds-project

## Запуск сервера
1. `npm -g install lerna`
2. `npm run install`
3. `npm run assemble`
4. если assemble не пашет - вперед собирать все ручками, у меня только так заработало
5. `npm run start:bhs`
6. изменить set $udsRoot в файле uds-common.conf
7. Запустить Nginx командой `nginx -c <путь до uds-project>/packages/nginx-conf/uds-common.conf`
8. Зайти в `apps/middle` и запустить команду `npm start`
9. Открыть в браузере localhost:8099
