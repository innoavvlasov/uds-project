const { Given, When, Then } = require('cucumber');
const { By, until } = require('selenium-webdriver');

Given('I am at authorization screen', async function() {
    await this.driver.get('http://localhost:3002');
});

When('Page is loaded', async function() {
    return this.driver.sleep(300);
});

Then('Element with id {string} is visible', function(elementId) {
    return this.waitForElement(By.id(elementId))
});

Then('Click on element with id {string}', async function(elementId) {
    const element = this.driver.findElement(By.id(elementId));
    await this.driver.wait(until.elementIsVisible(element), 1000);
    element.click();
});
