const { setWorldConstructor, After } = require('cucumber');
const { Builder, until } = require('selenium-webdriver');

function CustomWorld() {
  this.driver = new Builder().forBrowser('chrome').build();

  // Returns a promise that resolves to the element
  this.waitForElement = function(locator) {
    var condition = until.elementLocated(locator);
    return this.driver.wait(condition);
  }
}

After(function() {
    this.driver.quit();
});

setWorldConstructor(CustomWorld);
