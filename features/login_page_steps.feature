Feature: Page is loaded

    Scenario: Just page is loaded
        Given I am at authorization screen
        When Page is loaded
        Then Element with id "main-page-link" is visible
        Then Click on element with id "main-page-link"
        When Page is loaded
        Then Element with id "main-page-top-slider-counter" is visible
