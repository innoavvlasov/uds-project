const path = require("path");

const unifySvgIds = require('./scripts/unify-svg-ids')

// Unify svg
unifySvgIds(path.resolve(__dirname, "src/assets"))

module.exports = {
    mode: 'development',
    entry: "./src/index.ts",
    output: {
        filename: "uds-ui.js",
        path: path.resolve(__dirname, "dist"),
        libraryTarget: 'umd',
        publicPath: '/extlib/uds-ui/'
    },

    resolve: {
        modules: ['node_modules', 'src'],
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css"]
    },
    context: __dirname,

    devtool: "cheap-eval-source-map",
    module: {
        rules: [
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
            { test: /\.(jpe?g|gif|png|svg|woff|ttf|eot|wav|mp3)$/, use: "file-loader" }
        ],
    },
    externals:{
        "react": "react",
        "react-dom": "react-dom",
        'styled-components': 'styled-components'
    }
};
