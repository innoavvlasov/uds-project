import styled from 'styled-components';

import { COLORS } from '../../constants/colors';

export const ButtonWrapper = styled.button`
 @media screen and (min-width: 810px) {
        width: auto;
      }
      width: 100%;
    border-radius: 28px;
    background-color: ${(props: { disabled: boolean }) => props.disabled ? COLORS.notActive : COLORS.main};
    padding: 13px 34px;
    color: ${COLORS.text.light};
    font-size: 18px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    outline: none;
    cursor: ${(props: { disabled: boolean }) => props.disabled ? 'not-allowed' : 'pointer'};
    border: none;
`;
