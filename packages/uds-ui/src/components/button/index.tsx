import * as React from 'react';

import { ButtonWrapper } from './styled';

interface ButtonProps extends React.HTMLProps<HTMLButtonElement> {}

// Кнопка.
export const Button: React.FunctionComponent<ButtonProps> = ({ children, ...rest }) => (
    <ButtonWrapper {...rest}>
        {children}
    </ButtonWrapper>
);
