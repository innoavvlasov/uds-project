import styled from 'styled-components';

import { COLORS } from '../../constants/colors';

export const Wrapper = styled.div`
    display: flex;
    justify-content: center;
    min-height: 150px;
    align-items: flex-end;
    height: 45vh;
`;

export const DualRing = styled.div`
    display: block;
    width: 64px;
    height: 64px;

  &:after {
    content: "";
    display: block;
    width: 46px;
    height: 46px;
    margin: 1px;
    border-radius: 50%;
    border: 5px solid ${COLORS.main};
    border-color: ${COLORS.main} transparent ${COLORS.main} transparent;
    animation: uds-dual-ring-loader 1.2s linear infinite;
  }

  @keyframes uds-dual-ring-loader {
    to {
      transform: rotate(360deg);
    }
  }
`;
