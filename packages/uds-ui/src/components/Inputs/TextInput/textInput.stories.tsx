import * as React from 'react';
import { storiesOf } from '@storybook/react';

import { TextInput } from '..';

class Autofocus extends React.Component {
    inputRef: React.RefObject<HTMLInputElement> = React.createRef();
    componentDidMount() {
        this.inputRef.current!.focus();
    }

    render () {
        return (
            <TextInput label="Фокусируемся на этом поле через ref" ref={this.inputRef} />
        );
    }
}

storiesOf('TextInput ✏', module)
    .add('with Label', () => <TextInput id="story-input" label="Укажите имя" />)
    .add('without Label', () => <TextInput id="story-input" />)
    .add('required', () => <TextInput id="story-input" required={true} label="Укажите имя" />)
    .add('focused', () => <Autofocus />)
    .add('with placeholder', () => <TextInput id="story-input" placeholder="Укажите имя" />)
    .add('with value', () => <TextInput value="Многостройный воод текста" />);
