import * as React from 'react';

import { Label, StyledTextArea, Wrapper } from '../common/styled';

/**
 * @prop {Function} [onChange] Обработчик изменения значения инпута.
 * @prop {React.Ref<HTMLTextAreaElement>} [innerRef] Ссылка на Dom элемент.
 */
interface TextAreaProps extends Omit<React.HTMLProps<HTMLTextAreaElement>, 'onChange'> {
    onChange?: (value: string) => void;
    innerRef?: React.Ref<HTMLTextAreaElement>;
}

const Input = ({ id, label, className, required, type = 'text', children, onChange, innerRef, ...rest }: TextAreaProps) => {
    const handleChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        const value = event.target.value;

        onChange && onChange(value);
    };

    return (
        <Wrapper className={className}>
            <Label htmlFor={id} required={required}>{label}</Label>
            <StyledTextArea ref={innerRef} id={id} {...rest} type={type} required={required} onChange={handleChange} />
        </Wrapper>
    );
};

const TextInput: React.ForwardRefExoticComponent<TextAreaProps> = React.forwardRef((props, ref) =>
    <Input innerRef={ref} {...props}/>);

export default  TextInput;