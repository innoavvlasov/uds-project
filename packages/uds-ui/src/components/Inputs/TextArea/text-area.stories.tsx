import * as React from 'react';
import { storiesOf } from '@storybook/react';

import { TextArea } from '..';

class Autofocus extends React.Component {
    inputRef: React.RefObject<HTMLTextAreaElement> = React.createRef();
    componentDidMount() {
        this.inputRef.current!.focus();
    }

    render () {
        return (
            <TextArea label="Фокусируемся на этом поле через ref" ref={this.inputRef} />
        );
    }
}

storiesOf('Text area input ✏', module)
    .add('with Label', () => <TextArea id="story-input" label="Укажите имя" />)
    .add('without Label', () => <TextArea id="story-input" />)
    .add('required', () => <TextArea id="story-input" required={true} label="Укажите имя" />)
    .add('focused', () => <Autofocus />)
    .add('with placeholder', () => <TextArea id="story-input" placeholder="Укажите имя" />)
    .add('with value', () => <TextArea value={`Многостройный воод текста\nНовая строка`}/>);
