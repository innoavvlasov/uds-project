import * as React from 'react';
import { StyledDynamicTypography } from './styled';
import { PropsWithoutRef } from 'react';
import { RefAttributes } from 'react';

interface TypographyBaseProps extends React.HTMLProps<HTMLElement> {
  component?: React.ElementType<React.HTMLAttributes<HTMLElement>>;
  className?: string;
  variant?: TypographyVariantsType;
  fontSize?: string;
  fontWeight?: 400 | 600 | 700;
  color?: string;
  textTransform?: string;
  margin?: string;
  padding?: string;
  opacity?: string;
  innerRef?: any;
}

type TypographyVariantsType = 'description' | 'primary';

type VariantMapType = {
  [k in TypographyVariantsType]?: React.ElementType<React.HTMLAttributes<HTMLElement>>;
};

const variantsMap: VariantMapType = {
  description: 'span',
};

export const TypographyBase: React.FunctionComponent<TypographyBaseProps> = (props) => {
  const { component, className, variant, innerRef, fontSize, color, textTransform,
    margin, padding, opacity, fontWeight, ...others } = props;

  const Component =
    component ||
    (variant && variantsMap[variant]) ||
    'span';

  return (
    <StyledDynamicTypography
      as={Component}
      variant={variant}
      className={className}
      fontSize={fontSize}
      color={color}
      textTransform={textTransform}
      margin={margin}
      padding={padding}
      fontWeight={fontWeight}
      opacity={opacity}
      ref={innerRef}
      {...others}
    />
  );
};

interface TypographyProps extends Omit<TypographyBaseProps, 'innerRef' | 'ref'>, React.RefAttributes<HTMLDivElement> {}

/**
 * Компонент, который задаёт для себя стилизацию шрифтов. По умолчанию Typography является span.
 * @prop {string} component: Html element который будет использоваться как Typography element
 * @prop {string} variant: Вариация предустановленных стилей
 * @prop {string} className:  Имя класса css, который будет прокинут
 * @prop {string} fontSize: Переопределить размер шрифта (Например: '10px')
 * @prop {number} fontWeight: Переопределить толщину шрифта (Например: 400)
 * @prop {string} color: Переопределить цвет
 * @prop {string} textTransform: Переопределить стиль текста
 * @prop {string} margin: Переопределить оступы
 * @prop {string} padding: Переопределить внутренние отступы
 * @prop {string} opacity: Переопределить прозрачность
 * @prop {any} innerRef: React Ref к Typography element
 */
export const Typography: React.ForwardRefExoticComponent<PropsWithoutRef<TypographyProps> & RefAttributes<HTMLElement>> = React.forwardRef((props, ref) =>
  <TypographyBase innerRef={ref} {...props}/>);

Typography.displayName = 'Typography';
