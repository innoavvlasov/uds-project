import * as React from 'react';
import { storiesOf } from '@storybook/react';

import { TypographyBase } from '.';

storiesOf('Typography', module)
  .add('component', () => (
    <div>
      <TypographyBase>Default typography (equals component='span')</TypographyBase><br/>
      <TypographyBase component={'h1'}>Typography component='h1'</TypographyBase><br/>
      <TypographyBase component={'h2'}>Typography component='h2'</TypographyBase><br/>
      <TypographyBase component={'h3'}>Typography component='h3'</TypographyBase><br/>
      <TypographyBase component={'p'}>Typography component='p'</TypographyBase>
    </div>
  ))
  .add('variant', () => (
    <div>
      <TypographyBase variant={'description'}>Typography variant='description'</TypographyBase><br/>
      <TypographyBase component={'p'} variant={'description'}>Typography component='p' variant='description'</TypographyBase><br/>
      <TypographyBase variant={'primary'}>Typography variant='primary'</TypographyBase><br/>
    </div>
  ))
  .add('color', () => (
    <div>
      <TypographyBase color={'red'}>Typography color='red'</TypographyBase><br/>
      <TypographyBase color={'#3ef456'}>Typography color='#3ef456'</TypographyBase><br/>
      <TypographyBase color={'rgba(30,20,180,0.8)'}>Typography color='rgba(30,20,180,0.8)'</TypographyBase>
    </div>
  ))
  .add('font-size', () => (
    <div>
      <TypographyBase fontSize={'10px'}>Typography fontSize='10px'</TypographyBase><br/>
      <TypographyBase fontSize={'30px'}>Typography fontSize='30px'</TypographyBase>
    </div>
  ))
  .add('font-weight', () => (
    <div>
      <TypographyBase fontWeight={400}>Typography fontSize=400</TypographyBase><br/>
      <TypographyBase fontWeight={600}>Typography fontSize=600</TypographyBase><br/>
      <TypographyBase fontWeight={700}>Typography fontSize=700</TypographyBase>
    </div>
  ))
  .add('text-transform', () => (
    <div>
      <TypographyBase textTransform={'uppercase'}>Typography textTransform='uppercase'</TypographyBase><br/>
      <TypographyBase textTransform={'lowercase'}>Typography textTransform='lowercase'</TypographyBase><br/>
      <TypographyBase textTransform={'capitalize'}>Typography textTransform='capitalize'</TypographyBase><br/>
    </div>
  ));
