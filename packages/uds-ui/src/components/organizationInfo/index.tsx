import * as React from 'react';

import { LeftSection, MainSection, RightSection, Title, MainText, BottomText, BigIcon, ButtonWrapper } from './styled';
import { Button } from '../../components/button';

/**
 * @prop {string} title Заголовок.
 * @prop {string} mainText Основной текст.
 * @prop {string} bottomText жирный текст под [bottomText].
 * @prop {string} buttonText текст кнопки, что находиться под [buttonText].
 * @prop {string} src ссылка на картинку.
 * @prop {function} [onClick] Обработчки нажатия на кнопку.
 */
interface OrganizationInfoProps extends React.HTMLProps<HTMLButtonElement> {
    title: string;
    mainText: string;
    bottomText: string;
    buttonText: string;
    src: string;
}

/**
 * Компонент, как правило, использующийся, чтобы отображать информацию про учреждение
 * Состоит из двух чатей:
 *     Левая часть (колонкой):
 *         заголовок
 *         текст
 *         жирный текст
 *         кнопка
 *     Правая часть: изображение
 */
export const OrganizationInfo: React.FunctionComponent<OrganizationInfoProps> = ({title, mainText, bottomText, buttonText, onClick, src}) => (
    <MainSection>
        <LeftSection>
            <Title>
                {title}
            </Title>
            <MainText>
                {mainText}
            </MainText>
            <BottomText>
                {bottomText}
            </BottomText>
            <div>
                <Button onClick={onClick}>
                    {buttonText}
                </Button>
            </div>
        </LeftSection>
        <RightSection>
            <BigIcon src={src}/>
        </RightSection>
    </MainSection>
);
