import styled from 'styled-components';

import { COLORS } from '../../constants/colors';

export const MainSection = styled.section`
    @media screen and (min-width: 810px) {
      grid-template-columns: minmax(300px, 554px) minmax(384px, 529px);
      grid-template-rows: auto;
      padding: 0px 30px;
      gap: 0px 0px;
    }
    font-family: Montserrat, sans-serif;
    display: grid;
    grid-template-columns: calc(100% - 11px);
    grid-template-rows: 180px 1fr 50px;
    font-family: Montserrat, sans-serif;
    color: black;
    max-width: 1440px;
    max-height: 771px;
    align-self: center;
    justify-self: center;
    margin-bottom: 47px;
    gap: 24px 24px;
    padding: 0px 20px;
`;

export const LeftSection = styled.section`
    display: grid;
    grid-template-rows: auto auto auto;
    padding-top: calc(40px + (90 * ((100vw - 768px) / 672)));
    grid-row-gap: calc(10px + (30 * ((100vw - 768px) / 672)));
    grid-column: 1 / auto;
    grid-row: 2 / auto;
    @media screen and (min-width: 810px) {
      grid-row: 1 / auto;
    }
`;

export const RightSection = styled.section`
    @media screen and (min-width: 810px) {
      grid-column: 2 / auto;
      grid-row: 1 / auto;
    }
    max-width: 1440px;
    display: grid;
    -webkit-box-pack: center;
    justify-content: center;
    opacity: 1;
    grid-column: 1 / auto;
    grid-row: 1 / auto;
    margin-top: 80px;
`;

export const Title = styled.section`
      @media screen and (min-width: 810px) {
        transform: translateY(0px);
        font-size: calc(32px + (36 * ((100vw - 768px) / 672)));
      }
      font-size: 22px;
      font-weight: bold;
      font-style: normal;
      font-family: Montserrat, sans-serif;
      font-stretch: normal;
      line-height: 1;
      letter-spacing: normal;
      color: rgb(30, 29, 32);
      opacity: 1;
    
`;

export const MainText = styled.div`
      @media screen and (min-width: 810px) {
        transform: translateY(0px);
        margin-top: 0px;
        font-size: calc(12px + (4 * ((100vw - 768px) / 672)));
      }
      font-size: 16px;
      margin-top: 8px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: rgb(30, 29, 32);
      opacity: 1;
    
`;

export const BottomText = styled.div`
    margin-top: 24px;
    font-size: 16px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.13;
    letter-spacing: normal;
    color: ${COLORS.text.dark};
`;

export const BigIcon = styled.img`

`;

export const ButtonWrapper = styled.button`
 @media screen and (min-width: 810px) {
        width: auto;
      }
      width: 100%;
`;
