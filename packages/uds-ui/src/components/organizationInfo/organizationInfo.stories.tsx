import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { OrganizationInfo } from '.';

storiesOf('OrganizationInfo', module)
    .add('with text', () => (
        <OrganizationInfo bottomText="г.Москва, ЮЗАО, ул. Скобелевская, д. 23, к. 4"
            buttonText="Записаться"
            mainText="С 2006 года базовый центр района по организации досуга и спортивных занятий для жителей. "
            src="https://im0-tub-ru.yandex.net/i?id=655bb4a1ed3844a3a24da34dee59ebd2&n=13&exp=1"
            title="ГБУ ЦДиК «Южное Бутово»"
            onClick={action('button-click')}
        />
    ));