import * as React from 'react';
import { SliderBaseProps } from './slider-base';

import { Clicker, ControlContainer, Image, ImageContainer } from './styled';

export class Slider<S> extends React.Component<SliderBaseProps<S>> {
    state = {
        position: 0,
    };
    nextSlide = () => {
        const {position} = this.state;
        const {array} = this.props;
        this.setState({position: position === array.length - 1 ? 0 : position + 1});
    }

    prevSlide = () => {
        const {position} = this.state;
        const {array} = this.props;
        this.setState({position: position === 0 ? array.length - 1 : position - 1});
    }

    setSlide = (position: number) => {
        this.setState({position});
    }

    componentDidUpdate(prevProps: SliderBaseProps<S>) {
        if (prevProps.array !== this.props.array) {
            this.setSlide(0);
        }
    }

    render() {
        return (
            <ImageContainer>
                <Image
                    src={this.props.array[this.state.position]}
                />
                <ControlContainer>
                    <svg width="32" height="32" viewBox="0 0 64 64" className="sliderArrow" onClick={this.prevSlide}>
                        <path
                            fill="#003eff"
                            d="M0 32C0 14.35 14.35 0 32 0s32 14.35 32 32-14.35 32-32 32S0 49.65 0 32z"
                        />
                        <path
                            fill="#fff"
                            d="M22.674 33.219A2.352 2.352 0 0 1 22 31.602c0-.606.27-1.213.674-1.617l12.328-12.328c.876-.876 2.358-.876 3.234 0 .876.876.876 2.358 0 3.234L27.592 31.602l10.644 10.644a2.19 2.19 0 0 1 .673 1.617c0 .606-.202 1.213-.673 1.617-.876.876-2.358.876-3.234 0L22.674 33.219z"
                        />
                        <style>{`
                            .sliderArrow {
                                opacity: 0.2;
                                cursor: pointer;
                            }
                            .sliderArrow:hover {
                                opacity: 1;
                            }
                            .sliderArrow.inverted {
                                transform: rotate(180deg);
                            }
                         `}</style>
                    </svg>

                    {this.props.array.map((val, i) => (
                        <Clicker key={i} active={i === this.state.position} onClick={() => { this.setSlide(i); }}/>
                    ))}

                    <svg width="32" height="32" viewBox="0 0 64 64" className="sliderArrow inverted" onClick={this.nextSlide}>
                        <path
                            fill="#003eff"
                            d="M0 32C0 14.35 14.35 0 32 0s32 14.35 32 32-14.35 32-32 32S0 49.65 0 32z"
                        />
                        <path
                            fill="#fff"
                            d="M22.674 33.219A2.352 2.352 0 0 1 22 31.602c0-.606.27-1.213.674-1.617l12.328-12.328c.876-.876 2.358-.876 3.234 0 .876.876.876 2.358 0 3.234L27.592 31.602l10.644 10.644a2.19 2.19 0 0 1 .673 1.617c0 .606-.202 1.213-.673 1.617-.876.876-2.358.876-3.234 0L22.674 33.219z"
                        />
                    </svg>

                </ControlContainer>
            </ImageContainer>
        );
    }
}
