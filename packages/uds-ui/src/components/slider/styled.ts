import styled from 'styled-components';

export const Image = styled.img`
    max-width: calc(384px + (282 * ((100vw - 768px) / 672)));
    max-height: calc(355px + (261 * ((100vw - 768px) / 672)));
`;

export const ImageContainer = styled.div`
    max-width: 1440px;
    opacity: 1;
    transition: opacity 0.05s cubic-bezier(0.6, 0.18, 0.86, 0.69) 0s;
`;

export const ControlContainer = styled.div`
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 40px auto 0;
`;

export const Wrapper = styled.div`
`;

export const Clicker = styled.div`
    cursor: pointer;
    width: 12px;
    height: 12px;
    background-color: ${props => props.active ? 'rgb(0, 62, 255)' : 'rgb(219, 219, 219)'};
    border-radius: 50%;
    margin: 0px 4px;
    
    &:nth-child(2) {
        margin-left: 40px;
    }
    &:nth-last-child(2) {
        margin-right: 40px;
    }
`;
