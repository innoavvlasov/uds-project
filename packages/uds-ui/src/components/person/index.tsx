import * as React from 'react';

import { avaMale, avaFemale } from '../../assets';

import {
    PersonContainerColumn, PersonContainerRow, PersonAvatarContainerHorizontal, PersonAvatarContainerVertical, CenterText, PersonName, PersonJobTitle,
    PersonJobDescription, PersonColumnDescriptionContainer, PersonImg
} from './styled';

interface PersonProps {
    name: string;
    jobTitle: string;
    gender?: 'female' | 'male';
    image?: string;
    jobDescription?: string;
}

const DefaultImage = {
    female: avaFemale,
    male: avaMale
};

export const PersonVertical: React.FunctionComponent<PersonProps> = ({ image, name, jobTitle, gender }) => (
    <PersonContainerColumn>
        <PersonAvatarContainerVertical>
            <PersonImg
                src={image || DefaultImage[gender]}
            />
        </PersonAvatarContainerVertical>
        <PersonName>
            <CenterText>
                {name.split(' ')[0] + ' ' + name.split(' ')[1]}
            </CenterText>
            <br/>
            <CenterText>
                {name.split(' ')[2]}
            </CenterText>
        </PersonName>
        <PersonJobDescription>
            <CenterText>
                {jobTitle}
            </CenterText>
        </PersonJobDescription>
    </PersonContainerColumn>
);

export const PersonHorizontal: React.FunctionComponent<PersonProps> = ({image, name, jobTitle, jobDescription, gender}) => (
    <PersonContainerRow>
        <PersonAvatarContainerHorizontal>
            <PersonImg
                src={image || DefaultImage[gender]}
            />
        </PersonAvatarContainerHorizontal>
        <PersonColumnDescriptionContainer>
            <PersonName>
                {name}
            </PersonName>
            <PersonJobTitle>
                {jobTitle}
            </PersonJobTitle>
            <PersonJobDescription>
                {jobDescription}
            </PersonJobDescription>
        </PersonColumnDescriptionContainer>
    </PersonContainerRow>
);