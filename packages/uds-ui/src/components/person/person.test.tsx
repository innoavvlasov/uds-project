import * as React from 'react';
import renderer from 'react-test-renderer';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { exampleAva } from '../../assets';

configure({adapter: new Adapter()});

import { PersonVertical, PersonHorizontal } from '.';

describe('Person components', () => {
    it('Отрисовка с дефолтными картинками', () => {
        const horizontal = renderer.create(<PersonHorizontal name="Иванов Иван" jobTitle="Занятия: Раннее развитие с элементами айкидо для детей 5 – 17 лет"
                                                             jobDescription="Frontend фрилансер"
                                                             gender={'male'}/>).toJSON();
        const vertical = renderer.create(<PersonVertical name="Тестович Тест Тестов" jobTitle="Занятия: Раннее развитие с элементами айкидо для детей 5 – 17 лет"
                                                         gender={'female'}/>).toJSON();

        expect(horizontal).toMatchSnapshot();
        expect(vertical).toMatchSnapshot();
    });

    it('Отрисовка с картинками', () => {
        const chosenImage = require('../../assets/images/');
        const horizontal = renderer.create(<PersonHorizontal image={exampleAva} name="Иванов Иван"
                                                             jobTitle="Занятия: Раннее развитие с элементами айкидо для детей 5 – 17 лет"
                                                             jobDescription="Frontend фрилансер"
                                                             gender={'male'}/>).toJSON();
        const vertical = renderer.create(<PersonVertical image={exampleAva} name="Тестович Тест Тестов"
                                                         jobTitle="Занятия: Раннее развитие с элементами айкидо для детей 5 – 17 лет" gender={'female'}/>).toJSON();

        expect(horizontal).toMatchSnapshot();
        expect(vertical).toMatchSnapshot();
    });

});