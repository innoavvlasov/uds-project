import avaFemale from './ava-fem.svg';
import avaMale from './ava-mal.svg';

export {
    avaMale,
    avaFemale
};
