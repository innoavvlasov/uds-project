const dayjs = require('dayjs');

const getPath = path => `/api/v1${path}`;

const memory = {};
const getCashedValue = (name, calcFunction, args, expired = dayjs().add(1, 'day')) => {
    if (memory[name] && dayjs() < memory[name].expired) {
        console.info('returned cashed');
        return memory[name].value;
    }

    const value = calcFunction(...args);
    memory[name] = { value, expired };

    console.info('returned new');
    return value;
};

module.exports = {
    getPath,
    getCashedValue
};
