// Setting up the database connection
const knex = require('knex')({
  client: 'mysql2',
  connection: {
    host: 'localhost',
    user: 'UDSUSER',
    password: 'udsdeveloper',
    database: 'uds',
    port: '3307'
  }
})

const bookshelf = require('bookshelf')(knex)

module.exports = bookshelf
