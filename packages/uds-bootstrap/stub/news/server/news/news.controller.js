const express = require('express')
const router = express.Router()
const newsService = require('./news.service')
// const authorize = require('../_helpers/authorize')
// const Role = require('../_helpers/role')
const { check } = require('express-validator')
const validateRequest = require('../middlewares/validation')

// get news list
router.get('/', async (req, res, next) => {
  try {
    const newsList = await newsService.getNews()
    return newsList.length > 0 ? res.json(newsList) : res.status(404).json({})
  } catch (error) {
    return res.status(500).json({ message: 'Failed to load news.' })
  }
})

// get post
router.get('/:id', async (req, res, next) => {
  try {
    const post = await newsService.getPostById(req.params.id)
    return res.json(post)
  } catch (error) {
    if (error.message && error.message === 'EmptyResponse') {
      return res.status(404).json({ message: 'Post not found.' })
    }

    return res.status(500).json({ message: 'Failed to load news.' })
  }
})

// delete post by id
router.delete('/:id', async (req, res) => {
  try {
    await newsService.deleteById(req.params.id)
    return res.json({ success: true })
  } catch (error) {
    return res.status(500).json({ message: 'Failed to delete post.' })
  }
})

// validation rules for creating new post
const createNewsValidation = [
  check('title').exists(),
  check('content').exists(),
  check('author').exists(),
  check('tags').optional().isArray(),
  check('img').optional().isBase64()
]

// create new post
router.post('/', createNewsValidation, validateRequest, async (req, res) => {
  try {
    await newsService.createFromRequestBody(req.body)
    return res.json({ success: true })
  } catch (error) {
    return res.status(500).json({ message: 'Failed to create post.' })
  }
})

// validation rules for updating post
const updatePostValidation = [
  check('tags').optional().isArray(),
  check('img').optional().isBase64()
]

// create new post
router.put('/:id', updatePostValidation, validateRequest, async (req, res) => {
  try {
    await newsService.updateById(req.params.id, req.body)
    return res.json({ success: true })
  } catch (error) {
    return res.status(500).json({ message: 'Failed to update post.' })
  }
})

module.exports = router
