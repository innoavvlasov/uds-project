const express = require("express");
const router = express.Router();
const mainService = require("./main.service");

const authorize = require("../_helpers/authorize");
const Role = require("../_helpers/role");


router.get("/getMainData", authorize(Role.Admin), getMainData);
router.post("/saveVideoSection", authorize(Role.Admin), saveVideoSection);
module.exports = router;

async function getMainData(req, res, next) {
    await mainService
      .getMainData()
      .then(status => {
        return status
          ? res.json(status)
          : res.status(404).json({ message: "нет данных" });
      })
      .catch(err => next(err));
  }

  async function saveVideoSection(req, res, next) {
    await mainService
      .saveVideoSection(req.body)
      .then(status => {
        return status
          ? res.json(status)
          : res.status(404).json({ message: "данные не сохранены" });
      })
      .catch(err => next(err));
  }