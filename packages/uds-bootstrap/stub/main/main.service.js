const { DataBase } = require('../lib/db');

module.exports = {
    getMainData,
    saveVideoSection
};

async function getMainData() {

    const connection = new DataBase();
    await connection.init();
  
    const [mainData] = await connection.execute('SELECT * FROM uds.`main-uds`');
  
    connection.end();


    return ({
      status: {
        code:0
      },
      mainData: mainData[0]
    });
  }

  async function saveVideoSection(data) {

    console.log('data', data);
    const connection = new DataBase();
    await connection.init();
  
    // console.log('sql', `update uds.gbu set sections = '${JSON.stringify(data.sections)}' where url_page = '${data.pagename}'`);
  
    // const [res] = await connection.execute(`update uds.gbu set sections = JSON_INSERT('sections', '$.data', '') where url_page = ?`, [data.pagename]);
    const [res] = await connection.execute(`UPDATE uds.\`main-uds\` t SET t.videoSection = '${JSON.stringify(data)}' WHERE t.id = 1`);
  
    console.log('res', res);
  
    return data;
  }