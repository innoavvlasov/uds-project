import * as Redux from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import * as Thunk from 'redux-thunk';

import { workspace } from './workspace/reducers';

function createReducer(asyncReducers: any) {
  // console.log('asyncReducers', asyncReducers);
  return Redux.combineReducers({
    workspace,
    ...asyncReducers
  });
}

export async function injectAsyncReducer(store: any, name: any, asyncReducer: any) {
  store.asyncReducers[name] = asyncReducer;
  await store.replaceReducer(createReducer(store.asyncReducers));
}

export function configureStore(initialState: any = {}) {
  const store: any = Redux.createStore(
    createReducer({}),
    composeWithDevTools(Redux.applyMiddleware(Thunk.default))
  );
  store.asyncReducers = {};
  return store;
}
