import React, { useContext } from "react";


import { socialLinks } from './constants';

import { Header } from './components/header';
import { Footer } from './components/footer';


export const Wrapper = (props) => (<React.Fragment>
        <Header {...props} />
        {props.children}
        <Footer links={socialLinks} />
    </React.Fragment>)
