import * as React from 'react';

import { LogoBlock, NavBlock, NavLink, PhoneBlock, PhoneText, Wrapper, NavLinkStub, MenuBlock, StyledLogo, StyledMenu } from './styled';
import { phone } from '../../assets';
const getConfigValue = (param) => window['config'] && window['config'][param];

const resourcesUrl = getConfigValue('resourcesUrl') || '';

export class Header extends React.PureComponent <any, any> {
  state = { menuOpen: false };

  handlerOpenMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen });
    document.body.style['overflow-y'] = !this.state.menuOpen ? 'hidden' : 'scroll';
  }

  navigateTo = (path) => {
    let _path = path;
    if (_path[0] === 'm') {
      this.handlerOpenMenu();
      _path = _path.substr(1);
    }

    // this.props.history.push(_path);
  }

  navTo = (path) => {
     window.history.pushState(
            { flowName: path },
            '',
            path
        );
    this.props.openApp(path);
  }

  render () {
    return (
    <Wrapper>
      <LogoBlock to={'/'}>

        {!this.state.menuOpen && <StyledLogo id="logo" />}
        {this.state.menuOpen && 'Меню'}

      </LogoBlock>
      <div><StyledMenu onClick={this.handlerOpenMenu} open={this.state.menuOpen} /></div>
      <NavBlock>
        <NavLink id="main-page-link" onClick={() => this.navTo('uds-main')}  >Главная</NavLink>
        <NavLink onClick={() => this.navTo('uds-orgs/orgsApp')}>Учреждения</NavLink>
        <NavLink onClick={() => this.navTo('uds-news')} >Новости</NavLink>
        <NavLink onClick={() => this.navTo('uds-sections')} >Секции</NavLink>
        {/* <NavLinkStub >Рейтинги</NavLinkStub> */}
        <PhoneBlock>
          <div>
            <img src={`${resourcesUrl}/${phone}`} alt="phone" />
          </div>
          <PhoneText href="tel:+74952307071">8 (495) 230-70-71</PhoneText>
        </PhoneBlock>
      </NavBlock>
      {this.state.menuOpen && (<MenuBlock>

          <NavLink onClick={() => this.navigateTo('m/')}  >Главная</NavLink>
          <NavLink onClick={() => this.navigateTo('m/organizations')}  >Учреждения</NavLink>
          <NavLink onClick={() => this.navigateTo('/news')} >Новости</NavLink>
          <NavLink onClick={() => this.navigateTo('m/sections')}  >Секции</NavLink>
        <PhoneBlock>
          <div>
            <img src={`${resourcesUrl}/${phone}`} alt="phone" />
          </div>
          <PhoneText href="tel:+74952307071">8 (495) 230-70-71</PhoneText>
        </PhoneBlock>

      </MenuBlock>)}
    </Wrapper>
    );
  }
}
