export const SET_AREA = 'SET_AREA';
export const SET_DISTRICT = 'SET_DISTRICT';

// Запрос списка новостей.
export const NEWS_FETCH = 'NEWS_FETCH';
// Успешно завершенный запрос новостей.
export const NEWS_SUCCESS = 'NEWS_SUCCESS';
// Не успешно завершенный запрос новостей.
export const NEWS_FAILURE = 'NEWS_FAILURE';

// Запрос новости.
export const NEWS_ITEM_FETCH = 'NEWS_ITEM_FETCH';
// Успешно завершенный запрос новости.
export const NEWS_ITEM_SUCCESS = 'NEWS_ITEM_SUCCESS';
// Не успешно завершенный запрос новости.
export const NEWS_ITEM_FAILURE = 'NEWS_ITEM_FAILURE';
