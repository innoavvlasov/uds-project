import * as React from "react";
import { connect, ReactReduxContext } from 'react-redux';
import { createBrowserHistory } from 'history';
const history = createBrowserHistory();

// Get the current location.
const location = history.location;

// Listen for changes to the current location.
const unlisten = history.listen((location, action) => {
    debugger
  // location is an object like window.location
  console.log(action, location.pathname, location.state);
});



// To stop listening, call the function returned from listen().
unlisten();

import { listen } from 'fbjs/lib/EventListener';


import { openApp, errorOpenApp } from './workspace/actions';
import { loadApp } from './utils'
import { injectAsyncReducer } from './store';

import { Wrapper } from './wrapper';

class Bootstrap extends React.PureComponent <any, any> {
    App: any;
    currentBundle: string = '';

    componentDidMount() {
        listen(window, 'popstate', this.onPopState);

    }

    onPopState = (event) => {
        this.props.openApp(window.location.pathname);
    }

    async downloader(bundleName: string) {
        let _bundleName = bundleName;
     console.log('bundleName1', bundleName);
        this.currentBundle = bundleName;
        this.App = await loadApp(_bundleName);
        const { store } = this.context;

        if(this.App && this.App.reducer) {
            injectAsyncReducer(store, 'app', this.App.reducer);
        }

        this.forceUpdate();
    }

    render () {
        let { props: { bundleName, errorbundleName }, App, state, currentBundle } = this;

        console.log('bundleName', bundleName);
        console.log('currentBundle', currentBundle);
        if (bundleName !== currentBundle) {
            this.downloader(bundleName);
            return (<Wrapper><div>Идет загрузка...</div></Wrapper>);
        }

        return !App ? <Wrapper><div>Идет загрузка...</div></Wrapper> : <Wrapper {...this.props}><App {...this.props}/></Wrapper>;
    }
}

Bootstrap.contextType = ReactReduxContext;

const mapStateToProps = (state) => {
    return {
        errorbundleName: state.workspace.errorbundleName,
        bundleName: window.location.pathname === '/' ?   'defaultApp' : window.location.pathname,
    }
};

const mapDispatchToProps = (dispatch) => ({
    openApp: (bundleName: string) => {window.history.pushState('page', 'Title', '/'+bundleName);dispatch(openApp(bundleName));},
    errorOpenApp: (bundleName: string) => dispatch(errorOpenApp(bundleName))
    // getWorkSpace: () => dispatch(getWorkSpace()),
});

export const LoaderApp: any = connect(mapStateToProps, mapDispatchToProps)(Bootstrap);
