const getConfigValue = (param) => window['config'] && window['config'][param];

const resourcesUrl = getConfigValue('resourcesUrl') || '';

export async function loadApp(bundleName) {
    // console.log(`./${bundleName}.js`);
    // @ts-ignore
  return await System.import(`http://localhost:3002/${bundleName}/index.js`)
        .then(result => {
            return result.default;
        })
        .catch(error => {
            console.info(`Загрузить ${bundleName} не удалось!`);
            console.info(`error in ${bundleName}`, error);
        });
}
