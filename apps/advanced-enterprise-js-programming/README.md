# UDS Sections

## Scripts

### Build

Build affects only front-end part of the application, and is based on webpack. Hot reloading and `webpack-dev-server` in general are not supported due to technical restrictions imposed by `uds-bootstrap` architecture.

Supported build types are 'development' and 'production'.

For a one-time build, execute `npm run build:<build_type>` where `<build_type>` is 'dev' for development and 'prod' for production, e.g.: `npm run build:dev`.

To continuously watch directories and rebuild upon changes, start the same script but with ':watch' suffix appended, e.g.: `npm run build:prod:watch`.

Build generated files to the `./dist` directory.

### Run

Back-end is represented by `stub` server, which plays the following roles:

 - serving static content from `'./dist`;
 - authenticating users for API requests;
 - proxying API requests to the real `uds-sections-backend` service (implemented as docker-compose service in separate repository)

Starting commands:

 - `npm run start` - start the server once.
 - `npm run start:watch` - start the server and watch for changes to automatically restart.

Look into console output or server sources to find out its port number.

## Project structure

### '/' directory

 - `/src` - front-end source code & assets.
 - `/stub` - temporary `nodemon` server source code (until we got something better based on `nginx`).
 - `/dist` - build output, static content serving root.
 - `/webpack.config.js` - webpack configuration for development server.
 - `/webpack.conf.prod.js` - webpack configuration for build which will be published on npm. 
 - `/tsconfig.json` - typescript configuration.
 - `/nodemon.json` - `nodemon` runner configuration.

### '/src' directory 

 - `/src/assets` - directory for images and etc.
 - `/src/components` - directory for reusable components, 
for example: CustomButton will be located at `/src/components/CustomButton` directory which will contain all files related to this CustomButton except assets. 
 - `/src/containers` - directory for pages, for example: LoginPage.
 - `/src/app.tsx` - application starts here.

## Configuration

### Stub Server

Environment variables:

 - `UPSTREAM_SECTIONS` should contain URL of the `uds-sections-backend` server. Default is string `"localhost:4000"`.
 - `JWT_SECRET` should contain secret key which is used to sign JSON Web Tokens for UDS users authentication. For now it is shared among all UDS related sections, but normally it would be extracted somewhere like in the imaginary `uds-auth` module. Default value is provided in the `npm run start` script, purely for convenience during development stage.