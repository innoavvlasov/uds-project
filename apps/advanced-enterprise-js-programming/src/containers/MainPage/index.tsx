import * as React from 'react';
import { connect } from 'react-redux';

import * as styles from './styles.scss';
import * as actions from 'store/actionCreators';
import * as network from 'store/network';
import { isAdmin } from 'utlis/admin';
import { AppStore } from 'store/reducer';
import { PageLoader } from 'uds-ui';
import { Section, SectionType } from 'entity';
import EditableText from 'components/EditableText';
import * as accept_button from '../../public/access_button.svg';
import Img from "components/Img";

interface Props {
    sections: {
        all?: Section[],
        byType?: SectionType[],
    };

    fetchSectionsAll: () => void;
    fetchSectionsByType: () => void;

    createNewSection: (id: number, name: string) => void;
    editSectionName: (id: number, value: string) => void;
    deleteSection: (section: Section) => void;

    createNewSectionType: (name: string) => void;
    editSectionsTypeName: (id: number, value: string) => void;
    deleteSectionType: (sectionType: SectionType) => void;

    changePage: (page: string) => void;
}

interface State {
    editors: object;
    newSectionType: string;
}

class MainPage extends React.PureComponent<Props, State> {
    state = {
        editors: {},
        newSectionType: ''
    };

    componentDidMount() {
        if (!this.props.sections.all) {
            this.props.fetchSectionsAll();
        }
        if (!this.props.sections.byType) {
            this.props.fetchSectionsByType();
        }
    }

    onSectionNameSaved(id: number) {
        return (newName: string) => {
            this.props.editSectionName(id, newName);
        };
    }

    onSectionTypeNameSaved(id: number) {
        return (newName: string) => {
            this.props.editSectionsTypeName(id, newName);
        };
    }

    onDeleteSectionType(sectionType: SectionType): () => void | undefined {
        return sectionType.sections.length === 0
            ? () => this.props.deleteSectionType(sectionType)
            : undefined;
    }

    detailsPageFor(section: Section): string {
        return `/sections/section/${section.id}`;
    }

    handleSportAdders(event: any, sectionType: any) {
        const { value: newSport } = event.target;
        const { editors } = this.state;
        editors[sectionType.id] = newSport;
        this.setState({ editors });
        this.forceUpdate();
    }

    handleSectionAdder(event: any) {
        const { value } = event.target;
        this.setState({ newSectionType: value });
    }

    onCreateSportButtonClick(sectionType: any) {
        const { id } = sectionType;
        const { editors } = this.state;
        const { createNewSection } = this.props;
        if (editors[id]) {
            createNewSection(id, editors[id]);
        }
    }

    onCreateSectionTypeButtonClick() {
        const { newSectionType } = this.state;
        const { createNewSectionType } = this.props;
        if (newSectionType) {
            createNewSectionType(newSectionType);
        }
    }

    // render methods
    private renderSportAddingInputField(sectionType: any) {
        const { editors } = this.state;
        const isNewSportAddingAllowed = sectionType && editors[sectionType.id];
        return (
            <div className={styles['sections-page__new-item-block']}>
                {/*TODO: clear on send */}
                <input
                    onChange={event => this.handleSportAdders(event, sectionType)}
                    className={styles['sections-page__new-item-input']}
                    placeholder={'Добавить спорт'}
                />
                <Img
                    alt={''}
                    src={accept_button}
                    onClick={() => isNewSportAddingAllowed && this.onCreateSportButtonClick(sectionType)}
                    className={isNewSportAddingAllowed
                        ? styles['sections-page__new-item-img']
                        : [
                            styles['sections-page__new-item-img'],
                            styles['sections-page__new-item-img_unallowed'],
                        ].join(' ')
                    }
                />
            </div>
        );
    }

    private renderNewSectionAddingInputField() {
        const { newSectionType } = this.state;

        return (
            <div
                className={[
                    styles['sections-page__new-item-block'],
                    styles['sections-page__new-item-block_by-type'],
                ].join(' ')}
            >
                <input
                    onChange={event => this.handleSectionAdder(event)}
                    className={styles['sections-page__new-item-input']}
                    placeholder={'Добавить новый тип секций'}
                />
                <Img
                    alt={''}
                    src={accept_button}
                    onClick={() => this.onCreateSectionTypeButtonClick()}
                    className={newSectionType
                        ? styles['sections-page__new-item-img']
                        : styles['sections-page__new-item-img'] + ' ' +
                        styles['sections-page__new-item-img_unallowed']
                    }
                />
            </div>
        );
    }

    private renderByTypeTable(sections: Section[], sectionType?: SectionType, columnsAmount: number = 4) {
        // TODO: do not render 'new' input element when rendering all sections (not a section type).
        let columns: Section[][] = [];
        for (let i = 0; i < columnsAmount; i++) {
            columns.push([]);
        }
        sections.forEach((section, index) =>
            columns[index % columnsAmount].push(section)
        );
        return (
            <div className={styles['sections-page__block']}>
                {sectionType && this.renderSectionType(sectionType)}
                {this.renderSportAddingInputField(sectionType)}
                {columns.map((column, index) => this.renderColumn(column, index))}
            </div>
        );
    }

    private renderColumn(column: Section[], columnIndex: number) {
        return (
            <div
                key={columnIndex}
                className={styles['sections-page__block-payload-column']}
            >
                {column.map((section) => this.renderSection(section))}
            </div>
        );
    }

    private renderSection(section: Section) {
        const { id, name } = section;
        return (
            <div
                key={id}
                className={styles['sections-page__block-payload-item']}
            >
                {!isAdmin()
                    ? name
                    : <EditableText
                        value={name}
                        toLink={() => this.props.changePage(this.detailsPageFor(section))}
                        onSave={this.onSectionNameSaved(id)}
                        onDelete={() => this.props.deleteSection(section)}
                    />}
            </div>
        );
    }

    private renderSectionType(sectionType: SectionType) {
        const { id, name } = sectionType;
        return (
            <div className={styles['sections-page__block-title']}>
                {!isAdmin()
                    ? name
                    : <EditableText
                        value={name}
                        onSave={this.onSectionTypeNameSaved(id)}
                        onDelete={this.onDeleteSectionType(sectionType)}
                    />}
            </div>
        );
    }

    render() {
        const { sections } = this.props;
        return (
            <main className={styles['main-container']}>
                <div className={styles['sections-page-title']}>Секции в Москве</div>
                <div className={styles['sections-page']}>
                    <div className={styles['sections-page__title']}>По типу</div>
                    {this.renderNewSectionAddingInputField()}
                    {sections.byType
                        ? sections.byType.map((sectionType) =>
                            <div key={sectionType.id}>
                                {this.renderByTypeTable(sectionType.sections, sectionType)}
                            </div>
                        )
                        : <PageLoader />}

                    <div className={styles['sections-page__title']}>По алфавиту</div>
                    {sections.all
                        ? this.renderByTypeTable(sections.all)
                        : <PageLoader />}
                </div>
            </main>
        );
    }
}

const mapStateToProps = (store: AppStore) => {
    if (!store.app) {
        return {
            sections: {
                all: null,
                byType: null
            }
        };
    }
    const { app: { sections: { all = null, byType = null } } = { sections: {} } } = store;
    return {
        sections: { all, byType },
    };
};

const mapDispatchToProps = {
    fetchSectionsAll: network.GetAllSection,
    fetchSectionsByType: network.GetOrderedByTypeSection,

    createNewSection: network.PostNewSection,
    editSectionName: network.PostEditSection,
    deleteSection: network.PostDeleteSection,

    createNewSectionType: network.PostNewSectionType,
    editSectionsTypeName: network.PostEditSectionType,
    deleteSectionType: network.PostDeleteSectionType,

    changePage: actions.setPage,
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
