import { DetailedSection } from 'entity';
import * as React from 'react';
import { connect } from 'react-redux';
import { PageLoader } from 'uds-ui';

import { AppStore } from 'store/reducer';
import EditableText from 'components/EditableText';
import * as selectors from '../../store/selectors';

import * as network from '../../store/network';
import { VideoWithInfo } from "../../components/videoWithInfo";
import SectionsSlider from "../../components/sectionsSlider";
import { Social } from "../../components/social";
import { Enrol } from "../../components/Enrol";
import { Button } from "uds-ui";

import * as styles from './styles.scss';
import { isAdmin } from '../../utlis/admin';

interface Props {
    sectionUrl: string;
    fetchSection: (id: number) => void;
    section?: DetailedSection;
    setSection: (id: number, type: any, value: any) => void;
}

class SectionPage extends React.PureComponent<Props> {
    async componentDidMount() {
        const {sectionUrl} = this.props;
        
        const idRaw = sectionUrl.split('/').slice(-1)[0];
        let id = Number.parseInt(idRaw, 10);
        
        if (!Number.isInteger(id)) {
            console.log('SectionPage: bad id: ' + id);
        } else {
            this.props.fetchSection(id);
        }
    }

    onSaveButtonClick(sectionField: string) {
        return (value: string) => {
            let section = this.props.section;
            this.props.setSection(section.id, sectionField, value);
        };
    }

    render() {
        const {section} = this.props;
        if (!section) {
            return <PageLoader/>;
        }
        return (
            <main className={styles['sect-page']}>
                <section className={styles['sect-page__descr']}>
                    <div className={styles['sect-page__descr-info']}>
                        <div className={styles['sect-page__descr-info_title']}>
                            {!isAdmin()
                                ? section.name
                                : <EditableText
                                    value={section.name}
                                    onSave={this.onSaveButtonClick('name')}
                                />}
                        </div>
                        <div className={styles['sect-page__descr-info_text']}>
                            {!isAdmin()
                                ? section.main_description
                                : <EditableText
                                    value={section.main_description}
                                    onSave={this.onSaveButtonClick('main_description')}
                                />}

                        </div>
                        <div>
                            <Button>Подробнее</Button>
                        </div>

                    </div>
                    <div className={styles['sect-page__descr-image-box']}>
                    <img
                        className={styles['sect-page__descr-image']}
                        src={section.main_pic_url}
                        alt="Фото с ногой, пинающей футбольный мяч"
                    />
                    </div>
                </section>
                <VideoWithInfo
                    src={section.video_url}
                    mainText={section.description_body}
                    title={section.description_title}
                    onTitleButtonClick={this.onSaveButtonClick('description_title')}
                    onBodyButtonClick={this.onSaveButtonClick('description_body')}
                />
                <SectionsSlider images={section.pictures}/>
                <Social/>
                <Enrol/>
            </main>
        );
    }
}

const mapStateToProps = (state: AppStore) => ({
    section: selectors.getCurrentSection(state),
});

const mapDispatchToProps = {
    fetchSection: network.GetSectionDetails,
    setSection: network.SetSectionDetails,
};

export default connect(mapStateToProps, mapDispatchToProps)(SectionPage);
