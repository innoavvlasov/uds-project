// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'sect-page': string;
  'sect-page__descr': string;
  'sect-page__descr-image': string;
  'sect-page__descr-image-box': string;
  'sect-page__descr-info': string;
  'sect-page__descr-info_text': string;
  'sect-page__descr-info_title': string;
}
declare const cssExports: CssExports;
export = cssExports;
