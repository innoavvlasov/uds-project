export interface Section {
    id: number;
    name: string;
}

export interface SectionType {
    id: number;
    name: string;
    sections: Section[];
}

export interface DetailedSection extends Section {
    id: any;
    main_description: string;
    main_pic_url: string;
    description_title: string;
    description_body: string;
    last_picture: string;
    pictures: string[];
    video_url: string;
}

/**
 * Refer to the corresponding DTO from `uds-sections-backend` package.
 */
export interface PositionedSection {
    section: Section;
    sectionTypeId: number;
    sectionTypeIndex: number;
    sectionAllIndex: number;
}

/**
 * Refer to the corresponding DTO from `uds-sections-backend` package.
 */
export interface PositionedSectionType {
    sectionType: SectionType;
    sectionTypeIndex: number;
}
