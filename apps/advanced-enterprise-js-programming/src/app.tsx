import * as React from 'react';
import { connect } from 'react-redux';
import reducer from './store/reducer';

import MainPage from 'containers/MainPage';
import SectionPage from 'containers/SectionPage';
import Breadcrumb from 'components/Breadcrumb';

import * as actions from 'store/actionCreators';
import * as selectors from './store/selectors';

class App extends React.Component<any, any> {
    render() {
        const { lastUrl } = this.props;

        return (
            <React.Fragment>
                <Breadcrumb />
                
                {!(lastUrl && lastUrl.length)
                    ? <MainPage />
                    : <SectionPage sectionUrl={lastUrl} />
                }
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state: any) => ({
    lastUrl: selectors.getLastUrl(state),
});

const mapDispatchToProps = (dispatch: any) => ({
    popPage: () => dispatch(actions.popPage())
});

const connectedApp = connect(mapStateToProps, mapDispatchToProps)(App);
connectedApp['reducer'] = reducer;
export default connectedApp;