import * as React from "react";

export default class Img extends React.PureComponent<any, any> {
    render() {
        const getConfigValue = (param) => window['config'] && window['config'][param];

        const resourcesUrl = getConfigValue('resourcesUrl') || '';
        return (
            <img
                alt={this.props.alt}
                src={`${resourcesUrl}/${this.props.src}`}
                onClick={this.props.onClick}
                className={this.props.className}
            />
        );
    }
}
