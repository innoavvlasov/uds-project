import React from 'react';

import { connect } from 'react-redux';
import { AppStore } from 'src/store/reducer';

import * as styles from './styles.scss';
import * as mainStyles from '../../containers/MainPage/styles.scss';

import * as actions from 'store/actionCreators';

interface State {

};

interface Props {
    section: any
    pageList: string,

    popPage: () => void,
};

const defaultRoute = 'uds-sections'

const slash = (
    <div className={styles['breadcrumb__item']}>/</div>
);

class Breadcrumb extends React.PureComponent<any, any> {
    // onClick action handler
    onSectionsClick = () => {
        // console.log('asdf');
        this.props.popPage();
    };

    // render function
    render() {
        const { section } = this.props;
        const itemStyles = styles['breadcrumb__item'] + ' ' + styles['breadcrumb__text'];
        const nonclickableText = styles['breadcrumb__nonclickable-text'];
        return (
            <div className={mainStyles['main-container']}>
                <div className={styles['breadcrumb']}>
                    <div onClick={this.onSectionsClick}
                        className={itemStyles}>
                        секции
                    </div>
                    <div className={styles['breadcrumb__arrow']}>
                        >
                    </div>
                    <div className={nonclickableText}>
                        {section ? section.name : null}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (store: AppStore) => {
    return store.app
        ? {
            section: store.app.section,
            pageList: store.app.pageList
        }
        : {
            section: '',
            pageList: ''
        };  
};

const mapDispatchToProps = {
    popPage: actions.popPage,
};

export default connect(mapStateToProps, mapDispatchToProps)(Breadcrumb);