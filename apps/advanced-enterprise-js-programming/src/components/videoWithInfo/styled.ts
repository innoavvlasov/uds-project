import styled from 'styled-components';

export const MainSection = styled.section`
    @media screen and (min-width: 810px) {
      margin-top: 0px;
      flex-direction: row;
    }
    font-family: Montserrat, sans-serif;
    font-size: 12px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.17;
    letter-spacing: normal;
    margin-bottom: 104px;
    display: flex;
    align-self: center;
    -webkit-box-align: center;
    align-items: center;
    -webkit-box-pack: center;
    justify-content: center;
    justify-self: center;
    max-height: 698px;
    flex-direction: column-reverse;
    margin-top: 64px;
    background-repeat: no-repeat;
    padding: 0px 20px;
`;

export const LeftSection = styled.section`
`;

export const RightSection = styled.section`
    -webkit-box-align: center;
    align-items: center;
    @media screen and (min-width: 810px) {
      max-width: 32%;
    }
`;

export const Title = styled.section`
      @media screen and (min-width: 810px) {
        font-size: calc(14.4px + (17.6 * ((100vw - 768px) / 672)));
      }
      font-size: 22px;
      margin-top: 15px;
      margin-bottom: calc(20px + (12 * ((100vw - 768px) / 672)));
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: rgb(30, 29, 32);
    
`;

export const MainText = styled.div`
      @media screen and (min-width: 810px) {
        font-size: calc(12px + (4 * ((100vw - 768px) / 672)));
      }
      font-size: 16px;
      margin-top: 8px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: rgb(30, 29, 32);
      opacity: 1;
    
`;

export const VideoPlayer = styled.video`
          @media screen and (min-width: 810px) {
      min-width: 38.75%;
      margin-top: 56px;
      height: calc(220px + (178 * ((100vw - 768px) / 672)));
      margin-right: calc(40px + (83 * ((100vw - 768px) / 672)));
    }
    margin-top: 32px;
    min-width: 272px;
    height: 194px;
    opacity: 1;
    box-shadow: rgba(24, 40, 151, 0.4) 0px 16px 64px 0px;
    max-width: 558px;
    max-height: 398px;
    display: grid;
    font-size: calc(80px + (40 * ((100vw - 768px) / 672)));
    border-radius: 16px;
`;

export const Vector = styled.svg`
      align-self: center;
      justify-self: center;
      width: 1em;
      height: 1em;
      opacity: 0.8;
      transition: opacity 0.2s ease-in-out 0s;
`;
