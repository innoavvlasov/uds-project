import * as React from 'react';

import {LeftSection, MainSection, RightSection, Title, MainText, VideoPlayer, Vector} from './styled';
import {isAdmin} from "../../utlis/admin";
import EditableText from 'components/EditableText';

/**
 * @prop {string} title Заголовок.
 * @prop {string} mainText Основной текст.
 * @prop {string} src ссылка на видео.
 * @prop {function} [onClick] Обработчки нажатия на кнопку.
 */
interface VideoWithInfoProps extends React.HTMLProps<HTMLButtonElement> {
    title: string;
    mainText: string;
    src: string;
    onTitleButtonClick: (string) => void;
    onBodyButtonClick: (string) => void;
}

/**
 * Компонент, отображающий видео и дополнительную информацию к нему.
 * Состоит из двух чатей:
 *     Левая часть :
 *         видео
 *     Правая часть:
 *         заголовок
 *         текст
 */
export const VideoWithInfo: React.FunctionComponent<VideoWithInfoProps> = ({title, mainText, src, onTitleButtonClick,
                                                                               onBodyButtonClick}) => (
    <MainSection>
        <LeftSection>
            <VideoPlayer controls={true}>
                <source src={src} type="video/mp4"/>
                <track src="" kind="captions" srcLang="en" label="english_captions"/>
            </VideoPlayer>
        </LeftSection>
        <RightSection>
            <Title>
                {!isAdmin()
                    ? {title}
                    : <EditableText
                        value={title}
                        onSave={onTitleButtonClick}
                    />}
            </Title>
            <MainText>
                {!isAdmin()
                    ? {mainText}
                    : <EditableText
                        value={mainText}
                        onSave={onBodyButtonClick}
                    />}
            </MainText>
        </RightSection>
    </MainSection>
);
