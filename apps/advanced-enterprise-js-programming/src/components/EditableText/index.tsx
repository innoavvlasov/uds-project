import * as React from 'react';

import * as styles from './styles.scss';
import * as accept_button from '../../public/access_button.svg';
import * as edit_button from '../../public/edit_button.svg';
import * as cancel_button from '../../public/cancel_button.svg';
import * as delete_button from '../../public/trash_can.svg';
import Img from "components/Img";
import {TextArea} from 'uds-ui'

interface Props {
    toLink?: () => void;
    value: string;
    onSave: (value: string) => void;
    onDelete?: () => void;
}

interface State {
    value: string;
    isEditing: boolean;
    isDeletable: boolean;
}

export default class EditableText extends React.PureComponent<Props, State> {

    state = {
        value: this.props.value,
        isEditing: false,
        isDeletable: false,
    };

    static getDerivedStateFromProps(nextProps: Readonly<Props>, prevState: State): Partial<State> {
        // shield from updates while in editing mode.
        return prevState.isEditing ? null : {
            value: nextProps.value,
            isDeletable: Boolean(nextProps.onDelete),
        };
    }

    onEditingStart = () => {
        this.setEditing(true);
    };

    onEditingAccept = () => {
        // cache value before it is overridden by props
        const value = this.state.value;
        this.setEditing(false, () => {
            this.props.onSave(value);
        });
    };

    onEditingCancel = () => {
        this.setState({
            ...this.state,
            ...{
                isEditing: false,
                value: this.props.value,
            }
        });
    };

    setEditing(isEditing: boolean, callback?: () => void) {
        this.setState({
            ...this.state,
            ...{isEditing: isEditing}
        }, callback);
    }

    onValueStringChange = (str: string) => {
        this.setState(
            {
                ...this.state,
                ...{value: str}
            }
        );
    };

    onLinkClicked = (e: React.MouseEvent) => {
        e.preventDefault();
        if (this.props.toLink) {
            this.props.toLink();
        }
    };

    onDelete = (_e: React.MouseEvent) => {
        this.setEditing(false, () => {
            if (this.props.onDelete) {
                this.props.onDelete();
            }
        });
    };

    render() {
        return (
            <div className={styles['sections-editable-text_container']}>
                {!this.state.isEditing
                    ? <>
                        <Img
                            src={edit_button}
                            alt={'Редактировать'}
                            onClick={this.onEditingStart}
                            className={styles['sections-editable-text__button_img']}
                        />
                        {this.props.toLink
                            ? (
                                <a
                                    href="#"
                                    onClick={this.onLinkClicked}
                                    className={styles['sections-editable-text_non-editable_link']}
                                >
                                    {this.state.value}
                                </a>
                            )
                            : <span className={styles['sections-editable-text_non-editable']}>{this.state.value}</span>
                        }
                    </>
                    : <>
                        <Img
                            src={accept_button}
                            alt={'Сохранить'}
                            onClick={this.onEditingAccept}
                            className={styles['sections-editable-text__button_img']}
                        />
                        <TextArea
                            id={'description'}
                            value={this.state.value}
                            className={styles['sections-editable-text__input_field']}
                            onChange={this.onValueStringChange}
                            onKeyDown={e => e.keyCode === 13 && this.onEditingAccept()}
                        />
                        <Img
                            src={cancel_button}
                            alt={'Отменить'}
                            onClick={this.onEditingCancel}
                            className={styles['sections-editable-text__button_img']}
                        />
                        {this.state.isDeletable &&
                        <Img
                            src={delete_button}
                            alt={'Удалить'}
                            onClick={this.onDelete}
                            className={styles['sections-editable-text__button_img']}
                        />}
                    </>}
            </div>
        );
    }
}
