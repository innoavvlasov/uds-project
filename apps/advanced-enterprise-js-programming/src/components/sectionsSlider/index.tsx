import * as React from 'react';
import { MainSection, LeftArrow, SliderBox, RightArrow, SliderPhotos, Image } from './styled';
// import { SliderBase } from '../slider-base';
import { combineReducers } from 'redux';
import { Slide } from './slide';

interface SectionsSliderProps extends React.HTMLProps<HTMLButtonElement> {
    images: string[];
}

interface State {
    index: number;
}

export default class SectionsSlider extends  React.PureComponent<SectionsSliderProps, State> {

    state = {
        index: 0
    };

    onLeft = () => {
        this.setState({
            index: this.state.index - 1
        })
    };

    onRight = () => {
        this.setState({
            index: this.state.index + 1
        })
    };

    render() {
        return (<MainSection>
            <SliderBox>
                <LeftArrow width="32" height="32" viewBox="0 0 64 64" onClick={this.onLeft}>
                    <path
                        fill="#003eff"
                        d="M0 32C0 14.35 14.35 0 32 0s32 14.35 32 32-14.35 32-32 32S0 49.65 0 32z"
                    />
                    <path
                        fill="#fff"
                        d="M22.674 33.219A2.352 2.352 0 0 1 22 31.602c0-.606.27-1.213.674-1.617l12.328-12.328c.876-.876 2.358-.876 3.234 0 .876.876.876 2.358 0 3.234L27.592 31.602l10.644 10.644a2.19 2.19 0 0 1 .673 1.617c0 .606-.202 1.213-.673 1.617-.876.876-2.358.876-3.234 0L22.674 33.219z"
                    />
                </LeftArrow>
                <Slide key={0} image={this.props.images[this.state.index % this.props.images.length]}/>
                <RightArrow width="32" height="32" viewBox="0 0 64 64" onClick={this.onRight}>
                    <path
                        fill="#003eff"
                        d="M0 32C0 14.35 14.35 0 32 0s32 14.35 32 32-14.35 32-32 32S0 49.65 0 32z"
                    />
                    <path
                        fill="#fff"
                        d="M22.674 33.219A2.352 2.352 0 0 1 22 31.602c0-.606.27-1.213.674-1.617l12.328-12.328c.876-.876 2.358-.876 3.234 0 .876.876.876 2.358 0 3.234L27.592 31.602l10.644 10.644a2.19 2.19 0 0 1 .673 1.617c0 .606-.202 1.213-.673 1.617-.876.876-2.358.876-3.234 0L22.674 33.219z"
                    />
                </RightArrow>
            </SliderBox>
        </MainSection>);
    }
}

 const SectionsSlider1: React.FunctionComponent<SectionsSliderProps> = ({images}) => (

            <MainSection>
                <SliderBox>
                    <LeftArrow width="32" height="32" viewBox="0 0 64 64" onClick={''}>
                        <path
                            fill="#003eff"
                            d="M0 32C0 14.35 14.35 0 32 0s32 14.35 32 32-14.35 32-32 32S0 49.65 0 32z"
                        />
                        <path
                            fill="#fff"
                            d="M22.674 33.219A2.352 2.352 0 0 1 22 31.602c0-.606.27-1.213.674-1.617l12.328-12.328c.876-.876 2.358-.876 3.234 0 .876.876.876 2.358 0 3.234L27.592 31.602l10.644 10.644a2.19 2.19 0 0 1 .673 1.617c0 .606-.202 1.213-.673 1.617-.876.876-2.358.876-3.234 0L22.674 33.219z"
                        />
                    </LeftArrow>
                    {
                        images.map((image, i) => (
                            <Slide key={i} image={image}/>
                        ))
                    }
                    <RightArrow width="32" height="32" viewBox="0 0 64 64" onClick={''}>
                        <path
                            fill="#003eff"
                            d="M0 32C0 14.35 14.35 0 32 0s32 14.35 32 32-14.35 32-32 32S0 49.65 0 32z"
                        />
                        <path
                            fill="#fff"
                            d="M22.674 33.219A2.352 2.352 0 0 1 22 31.602c0-.606.27-1.213.674-1.617l12.328-12.328c.876-.876 2.358-.876 3.234 0 .876.876.876 2.358 0 3.234L27.592 31.602l10.644 10.644a2.19 2.19 0 0 1 .673 1.617c0 .606-.202 1.213-.673 1.617-.876.876-2.358.876-3.234 0L22.674 33.219z"
                        />
                    </RightArrow>
                </SliderBox>
            </MainSection>
        );