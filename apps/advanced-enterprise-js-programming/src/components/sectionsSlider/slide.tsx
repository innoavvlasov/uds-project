import React from 'react';
import { SliderPhotos, Image } from './styled';

interface SlideProps extends React.HTMLProps<HTMLButtonElement> {
    key: number;
    image: string;
}

export const Slide: React.FunctionComponent<SlideProps> = ({image, key}) => (
    <SliderPhotos key={key}>
        <Image src={image}/>
    </SliderPhotos>
);
