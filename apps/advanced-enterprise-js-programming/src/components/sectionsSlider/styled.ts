import styled from 'styled-components';

export const MainSection = styled.section`
    position: relative;
    font-family: Montserrat, sans-serif;
    font-size: 12px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.17;
    letter-spacing: normal;
    color: rgb(255, 255, 255);
    align-self: center;
    justify-self: center;
    max-width: 1400px;
    width: 100%;
    display: flex;
    flex-direction: column;
    -webkit-box-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    align-items: center;
    margin-bottom: calc(45px + (38 * ((100vw - 768px) / 672)));
`;

export const LeftArrow = styled.svg`
      @media screen and (min-width: 810px) {
        opacity: 1;
        width: 15%;
        padding-left: 9%;
      }
      width: 5%;
      z-index: 1;
      padding-left: 9%;
      opacity: 0;
`;

export const RightArrow = styled.svg`
      @media screen and (min-width: 810px) {
        opacity: 1;
        width: 15%;
        padding-left: 9%;
      }
      width: 5%;
      z-index: 1;
      padding-left: 9%;
      opacity: 0;
      transform: rotate(180deg);
`;

export const SliderBox = styled.div`
    position: relative;
    width: 100%;
    display: flex;
    -webkit-box-pack: justify;
    justify-content: space-between;
    -webkit-box-align: center;
    align-items: center;
    overflow: hidden;
`;

export const SliderPhotos = styled.div`
      @media screen and (min-width: 810px) {
        height: calc(400px + (150 * ((100vw - 768px) / 672)));
        left: ${props => props.key * 120}%;
      }
       display: flex;
        margin: 0 0 20px 20px;
      left: 0%;
      height: 230px;
      width: 100%;
      position: relative;
      background-color: rgb(255, 255, 255);
`;

export const Image = styled.img`
      @media screen and (min-width: 810px) {
      }
      width: 100%;
      height: 100%;
      position: absolute;
      top: 0px;
      opacity: 1;
      order: 2;
      left: 0%;
      display: block;
      transition: left 1s ease-in-out 0s, opacity 1s ease-in-out 0s;
`;