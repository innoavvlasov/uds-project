import * as React from 'react';

import { EnrDiv } from './styled';

import Img from "../Img";
import { Button } from './styled';


/**
 * Иконки социальных сетей:
 *         заголовок
 *         ряд иконок
 */
export const Enrol = () => (
    <EnrDiv>
        <div>Достигайте своих целей</div>
        <Button>Записаться</Button>
    </EnrDiv>
);
