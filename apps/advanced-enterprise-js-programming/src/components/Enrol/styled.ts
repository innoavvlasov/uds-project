import styled from 'styled-components';

export const EnrDiv = styled.div`
    @media screen and (min-width: 810px) {
        font-size: 50px;
        font-weight: bold;
    }
    font-family: Montserrat, sans-serif;
    font-size: 22px;
    font-weight: bold;
    margin-top: 85px;
    background-color: rgb(0, 62, 255);
    color: white;
    padding-top: 48px;
    padding-bottom: 40px;
    text-align: center;
    margin-bottom: 105px;
    border-radius: 16px;

`;
export const Button = styled.button`
    width: auto;
    border-radius: 28px;
    background-color: white;
    padding: 13px 34px;
    color: black;
    font-size: 18px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    outline: none;
    cursor: ${(props: { disabled: boolean }) => props.disabled ? 'not-allowed' : 'pointer'};
    border: none;
    `;