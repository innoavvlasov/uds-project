import * as React from 'react';

import * as phone_grey from '../../assets/phone_grey.svg';
import * as logo from '../../assets/logo_grey.svg';
import * as vk from '../../assets/g-vk.svg';
import * as instagram from '../../assets/g-instagram.svg';
import * as odnoklassniki from '../../assets/g-odnoklassniki.svg';
import * as facebook from '../../assets/g-facebook.svg';
import * as twitter from '../../assets/g-twitter.svg';

import * as styles from './styles.scss';

export default class SectionsApp extends React.PureComponent {
    renderLogo() {
        return (
            <span>
                <img src={logo} alt="logo"/>
            </span>
        );
    }

    render() {
        return (
            <footer className={styles['sections-footer']}>
                <a className={styles['sections-footer-el__logo']} href="/">
                    {this.renderLogo()}
                </a>
                <div className={styles['sections-footer-el__contacts']}>
                    <div>
                        <img src={phone_grey} alt="phone"/>
                    </div>
                    <a href="tel:+74952307071" className={styles['sections-footer-el__contacts_phone']}>8 (495) 230-70-71</a>
                    <div className={styles['sections-footer-el__contacts_social']}>
                        <a href="http://vk.com/uds_moscow" target="_blank">
                            <img src={vk} alt="vk"/>
                        </a>
                        <a href="https://www.instagram.com/uds_moscow/" target="_blank">
                            <img src={instagram} alt="instagram"/>
                        </a>
                        <a href="https://www.ok.ru/group/54615427383430/" target="_blank">
                            <img src={odnoklassniki} alt="odnoklassniki"/>
                        </a>
                        <a href="https://www.facebook.com/uds.moscow" target="_blank">
                            <img src={facebook} alt="facebook"/>
                        </a>
                        <a href="https://twitter.com/uds__moscow" target="_blank">
                            <img src={twitter} alt="twitter"/>
                        </a>
                    </div>
                </div>
            </footer>
        );
    }
}