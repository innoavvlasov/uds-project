import styled from 'styled-components';

export const MainSection = styled.section`

`;

export const Title = styled.div`
      @media screen and (min-width: 810px) {
        span:first-child {
          font-size: 38px;
          display: inline;
        }
        span:nth-child(2) {
          display: none;
        }
      }

      font-family: Montserrat, sans-serif;
      margin: 0px;
      padding: 0px;
      font-size: 22px;
      font-weight: 600;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.43;
      letter-spacing: normal;
      text-align: center;
      color: rgb(30, 29, 32);
`;

export const Icons = styled.div`
      @media screen and (min-width: 810px) {
        height: 192px;
        padding-top: 0px;
        -webkit-box-align: center;
        align-items: center;
        a:nth-child(n+2) {
          margin-left: 48px;
        }
      }
      width: 100%;
      display: flex;
      -webkit-box-pack: center;
      justify-content: center;
      -webkit-box-align: baseline;
      align-items: baseline;
      height: 100px;
      padding-top: 12px;
`;