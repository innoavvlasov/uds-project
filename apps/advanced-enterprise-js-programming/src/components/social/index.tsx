import * as React from 'react';

import { MainSection, Title, Icons } from './styled';
import * as fb from '../../assets/facebook.svg';
import * as vk from '../../assets/vk.svg';
import * as inst from '../../assets/instagram.svg';
import * as ok from '../../assets/odnoklassniki.svg';
import * as tw from '../../assets/twitter.svg';
import Img from "../Img";


/**
 * Иконки социальных сетей:
 *         заголовок
 *         ряд иконок
 */
export const Social = () => (
    <MainSection>
        <Title>
            <span>Мы в социальных сетях</span>
            <span>Мы в соцсетях</span>
        </Title>
        <Icons>
            <a href="http://vk.com/uds_moscow" target="_blank">
                <Img src={vk} alt="ссылка на сайт вконтакте"/>
            </a>
            <a href="https://www.instagram.com/uds_moscow/" target="_blank">
                <Img src={inst} alt="ссылка на сайт инстаграм"/>
            </a>
            <a href="https://www.ok.ru/group/54615427383430/" target="_blank">
                <Img src={ok} alt="ссылка на сайт одноклассники"/>
            </a>
            <a href="https://www.facebook.com/uds.moscow" target="_blank">
                <Img src={fb} alt="ссылка на сайт фейсбук"/>
            </a>
            <a href="https://twitter.com/uds__moscow" target="_blank">
                <Img src={tw} alt="ссылка на сайт твиттер"/>
            </a>
        </Icons>
    </MainSection>
);
