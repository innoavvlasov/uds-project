import * as React from 'react';

import * as phone from '../../assets/phone.svg';
import * as favicon from '../../assets/favicon.ico';
import * as styles from './styles.scss';

export default class SectionsApp extends React.PureComponent {
    renderLogo() {
        return (
            <div className={styles['sections-header-el__logo_els']}>
                <img src={favicon} alt="favicon"/>
                <span>Досуг и спорт</span>
            </div>
        );
    }

    render() {
        return (
            <header className={styles['sections-header']}>
                <a className={styles['sections-header-el__logo']} href="/">
                    {this.renderLogo()}
                </a>
                <div className={styles['sections-header-el_empty']}/>
                <div className={styles['sections-header-el__menu']}>
                    <span className={styles['sections-header-el__menu-text']}>Главная</span>
                    <span className={styles['sections-header-el__menu-text']}>Учреждения</span>
                    <span className={styles['sections-header-el__menu-text']}>Секции</span>
                    <div className={styles['sections-header-el__menu-phone']}>
                        <div><img src={phone} alt="phone"/></div>
                        <a
                            href="tel:+74952307071"
                            className={styles['sections-header-el__menu-phone_number']}
                        >
                            8 (495) 230-70-71
                        </a>
                    </div>
                </div>
            </header>
        );
    }
}