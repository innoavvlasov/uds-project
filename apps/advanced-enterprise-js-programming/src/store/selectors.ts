/* tslint:disable:no-bitwise */
import { Section, SectionType } from 'entity';
import { AppStore, Store } from 'store/reducer';

export const getLastUrl = (state: AppStore) => {
    return state.app && state.app.pageList
        ? state.app.pageList[state.app.pageList.length - 1]
        : null;
};

export const getCurrentSection = (state: AppStore) => {
    return state.app && state.app.section
        ? state.app.section
        : null;
};

function RawSectionIndexById(sections: Section[], sectionId: number): number {
    return sections.findIndex(s => s.id === sectionId);
}

function RawSectionTypeIndexById(sectionTypes: SectionType[], sectionTypeId: number): number {
    return sectionTypes.findIndex(st => st.id === sectionTypeId);
}

export function StoreAllSectionIndexById(store: Store, sectionId: number): number {
    try {
        return RawSectionIndexById(store.sections.all, sectionId);
    } catch {
        return -1;
    }
}

export function StoreSectionTypeIndexById(store: Store, sectionTypeId: number): number {
    try {
        return RawSectionTypeIndexById(store.sections.byType, sectionTypeId);
    } catch {
        return -1;
    }
}

/**
 * Select both section type index and index of section inside `SectionType.sections` array.
 */
export function StorePathToSectionById(store: Store, sectionId: number): { sectionTypeIndex: number, sectionIndex: number } {
    try {
        /* tslint:disable:no-shadowed-variable */
        for (let {st, i: sectionTypeIndex} of store.sections.byType.map((st, i) => ({st, i}))) {
            const sectionIndex = st.sections.findIndex(s => s.id === sectionId);
            if (~sectionIndex) {
                return {sectionTypeIndex, sectionIndex};
            }
        }
    } catch {
        return {sectionTypeIndex: -1, sectionIndex: -1};
    }
}
