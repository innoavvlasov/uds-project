import { DetailedSection, PositionedSection, PositionedSectionType, Section, SectionType } from 'entity';

/**
 * Typed actions as shown in this article: https://spin.atomicobject.com/2017/07/24/redux-action-pattern-typescript/
 */
export enum TypeKeys {

    /****************** Managing pages ******************/

    SET_SECTION = 'SECTIONS.APP.SET_SECTION',
    SET_PAGE = 'SECTIONS.APP.SET_PAGE',
    POP_PAGE = 'SECTIONS.APP.POP_PAGE',
    LOAD_FAIL = 'SECTIONS.APP.LOAD_FAIL',
    SET_BREADCRUMB = 'SECTIONS.APP.SET_BREADCRUMB',

    /****************** Loading ******************/

    LOAD_ALL = 'SECTIONS.APP.LOAD_ALL',
    LOAD_BY_TYPE = 'SECTIONS.APP.LOAD_BY_TYPE',

    /****************** Section ******************/

    NEW_SECTION = 'SECTIONS.APP.NEW_SECTION',
    EDIT_SECTION = 'SECTIONS.APP.EDIT_SECTION',
    DELETE_SECTION = 'SECTIONS.APP.DELETE_SECTION',

    /****************** SectionType ******************/

    NEW_SECTION_TYPE = 'SECTIONS.APP.NEW_SECTION_TYPE',
    EDIT_SECTION_TYPE = 'SECTIONS.APP.EDIT_SECTION_TYPE',
    DELETE_SECTION_TYPE = 'SECTIONS.APP.DELETE_SECTION_TYPE',

    /****************** Stub ******************/
    OTHER_ACTION = '__any_other_action_type__'
}

export namespace Actions {

    /****************** Managing pages ******************/

    export interface SetSection {
        type: TypeKeys.SET_SECTION;
        payload: DetailedSection;
    }

    export interface SetPage {
        type: TypeKeys.SET_PAGE;
        payload: string;
    }

    export interface PopPage {
        type: TypeKeys.POP_PAGE;
    }

    export interface LoadFail {
        type: TypeKeys.LOAD_FAIL;
        payload: any;
    }

    export interface SetBreadcrumb {
        type: TypeKeys.SET_BREADCRUMB;
        payload: any;
    }

    /****************** Loading ******************/

    export interface LoadAll {
        type: TypeKeys.LOAD_ALL;
        payload: Section[];
    }

    export interface LoadByType {
        type: TypeKeys.LOAD_BY_TYPE;
        payload: SectionType[];
    }

    /****************** Section ******************/

    export interface NewSection {
        type: TypeKeys.NEW_SECTION;
        payload: PositionedSection;
    }

    export interface EditSection {
        type: TypeKeys.EDIT_SECTION;
        payload: PositionedSection;
    }

    export interface DeleteSection {
        type: TypeKeys.DELETE_SECTION;
        payload: Section;
    }

    /****************** SectionType ******************/

    export interface NewSectionType {
        type: TypeKeys.NEW_SECTION_TYPE;
        payload: PositionedSectionType;
    }

    export interface EditSectionType {
        type: TypeKeys.EDIT_SECTION_TYPE;
        payload: PositionedSectionType;
    }

    export interface DeleteSectionType {
        type: TypeKeys.DELETE_SECTION_TYPE;
        payload: SectionType;
    }

    /****************** Stub ******************/

    export interface Other {
        type: TypeKeys.OTHER_ACTION;
        payload: any;
    }

    export type Types =
        | Other
        /****************** Managing pages ******************/
        | SetSection
        | SetPage
        | PopPage
        | LoadFail
        | SetBreadcrumb
        /****************** Loading ******************/
        | LoadAll
        | LoadByType
        /****************** Section ******************/
        | NewSection
        | EditSection
        | DeleteSection
        /****************** SectionType ******************/
        | NewSectionType
        | EditSectionType
        | DeleteSectionType;
}
