import { DetailedSection, PositionedSection, PositionedSectionType, Section, SectionType } from 'entity';
import * as constants from './constants';
import { Actions } from './constants';

/****************** Managing pages ******************/

export const popPage = (): Actions.PopPage => ({
    type: constants.TypeKeys.POP_PAGE,
});

export const setPage = (page: string): Actions.SetPage => ({
    type: constants.TypeKeys.SET_PAGE,
    payload: page
});

export const setSection = (section: DetailedSection): Actions.SetSection => ({
    type: constants.TypeKeys.SET_SECTION,
    payload: section
});

/**
 * General purpose error handler. Maybe should be replaced with something better.
 * @param reason anything that would fit in `console.log` or `window.alert`.
 */
export const loadFailed = (reason: any): Actions.LoadFail => ({
    type: constants.TypeKeys.LOAD_FAIL,
    payload: reason,
});

/****************** Loading ******************/

export const loadAll = (all: Section[]): Actions.LoadAll => ({
    type: constants.TypeKeys.LOAD_ALL,
    payload: all,
});

export const loadByType = (byType: SectionType[]): Actions.LoadByType => ({
    type: constants.TypeKeys.LOAD_BY_TYPE,
    payload: byType,
});

/****************** Section ******************/

export const newSection = (it: PositionedSection): Actions.NewSection => ({
    type: constants.TypeKeys.NEW_SECTION,
    payload: it
});

export const editSection = (it: PositionedSection): Actions.EditSection => ({
    type: constants.TypeKeys.EDIT_SECTION,
    payload: it,
});

export const deleteSection = (section: Section): Actions.DeleteSection => ({
    type: constants.TypeKeys.DELETE_SECTION,
    payload: section,
});

/****************** SectionType ******************/

export const newSectionType = (it: PositionedSectionType): Actions.NewSectionType => ({
    type: constants.TypeKeys.NEW_SECTION_TYPE,
    payload: it
});

export const editSectionType = (it: PositionedSectionType): Actions.EditSectionType => ({
    type: constants.TypeKeys.EDIT_SECTION_TYPE,
    payload: it,
});

export const deleteSectionType = (it: SectionType): Actions.DeleteSectionType => ({
    type: constants.TypeKeys.DELETE_SECTION_TYPE,
    payload: it
});


export const setBreadcrumb = (it: string) => ({
    type: constants.TypeKeys.SET_BREADCRUMB,
    payload: it
})