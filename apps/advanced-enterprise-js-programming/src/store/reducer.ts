/* tslint:disable:no-bitwise */
import { PositionedSection, PositionedSectionType, Section, SectionType } from 'entity';
import produce, { Draft } from 'immer';
import * as constants from './constants';
import { Actions } from './constants';
import * as selectors from './selectors';

export interface AppStore {
    app?: Store;
}

export interface Store {
    pageList: string[];
    section?: object;
    sections: {
        all?: Section[],
        byType?: SectionType[],
    };
}

const initialState: Store = {
    pageList: [],
    section: undefined,
    sections: {
        all: null,
        byType: null,
    },
};

// TODO: scroll to new/edited section

/****************** Section ******************/

function newSection(store: Store, positioned: PositionedSection): Store {
    // insert into new position
    return produce(store, (draft: Draft<Store>) => {
        // 1. inside all sections
        const all = draft.sections.all;
        all.splice(positioned.sectionAllIndex, 0, positioned.section);

        // 2. inside section type
        const sectionTypeIndex = selectors.StoreSectionTypeIndexById(store, positioned.sectionTypeId);
        if (~sectionTypeIndex) {
            const sectionType = draft.sections.byType[sectionTypeIndex];
            sectionType.sections.splice(positioned.sectionTypeIndex, 0, positioned.section);
        }
    });
}

function editSection(store: Store, positioned: PositionedSection): Store {
    // move from old position to the new one
    store = deleteSection(store, positioned.section);
    return newSection(store, positioned);
}

function deleteSection(store: Store, section: Section): Store {
    // remove from old position
    return produce(store, (draft: Draft<Store>) => {
        // 1. inside all sections
        const oldAllIndex = selectors.StoreAllSectionIndexById(store, section.id);
        if (~oldAllIndex) {
            draft.sections.all.splice(oldAllIndex, 1);
        }

        // 2. inside section type
        const path = selectors.StorePathToSectionById(store, section.id);
        if (~path.sectionTypeIndex && ~path.sectionIndex) {
            draft.sections.byType[path.sectionTypeIndex].sections.splice(path.sectionIndex, 1);
        }
    });
}

/****************** SectionType ******************/

function newSectionType(store: Store, positioned: PositionedSectionType): Store {
    return produce(store, (draft: Draft<Store>) => {
        draft.sections.byType.splice(positioned.sectionTypeIndex, 0, positioned.sectionType);
    });
}

function editSectionType(store: Store, positioned: PositionedSectionType): Store {
    store = deleteSectionType(store, positioned.sectionType);
    return newSectionType(store, positioned);
}

function deleteSectionType(store: Store, sectionType: SectionType): Store {
    return produce(store, (draft: Draft<Store>) => {
        const oldSectionTypeIndex = selectors.StoreSectionTypeIndexById(store, sectionType.id);
        if (~oldSectionTypeIndex) {
            draft.sections.byType.splice(oldSectionTypeIndex, 1);
        }
    });
}

export default (store: Store = initialState, action: Actions.Types): Store => {

    switch (action.type) {
        /****************** Managing pages ******************/

        case constants.TypeKeys.SET_SECTION:
            return produce(store, (draft: Draft<Store>) => {
                draft.section = action.payload;
            });

        case constants.TypeKeys.SET_PAGE:
            return produce(store, (draft: Draft<Store>) => {
                draft.pageList.push(action.payload);
            });

        case constants.TypeKeys.POP_PAGE:
            return produce(store, (draft: Draft<Store>) => {
                draft.pageList.splice(0, 2);
                delete(draft.section);
            });

        case constants.TypeKeys.LOAD_FAIL: {
            const reason = action.payload;
            window.alert(reason);
            return store;
        }

        case constants.TypeKeys.SET_BREADCRUMB: {
            return produce(store, (draft: Draft<Store>) => {
                
            });
        }

        /****************** Loading ******************/

        case constants.TypeKeys.LOAD_ALL:
            return produce(store, (draft: Draft<Store>) => {
                draft.sections.all = action.payload;
            });

        case constants.TypeKeys.LOAD_BY_TYPE:
            return produce(store, (draft: Draft<Store>) => {
                draft.sections.byType = action.payload;
            });

        /****************** Section ******************/

        case constants.TypeKeys.NEW_SECTION:
            return newSection(store, action.payload);

        case constants.TypeKeys.EDIT_SECTION:
            return editSection(store, action.payload);

        case constants.TypeKeys.DELETE_SECTION:
            return deleteSection(store, action.payload);

        /****************** SectionType ******************/

        case constants.TypeKeys.NEW_SECTION_TYPE:
            return newSectionType(store, action.payload);

        case constants.TypeKeys.EDIT_SECTION_TYPE:
            return editSectionType(store, action.payload);

        case constants.TypeKeys.DELETE_SECTION_TYPE:
            return deleteSectionType(store, action.payload);

        /****************** Stub ******************/

        default:
            return store;
    }
};
