import axios from 'axios';
import { DetailedSection, Section, SectionType } from 'entity';
import { Dispatch } from 'redux';
import * as creators from './actionCreators';

/* ================ Async dispatch ================ */
/** Swagger API implementation
 *
 * Functions' names are OperationID in terms of Swagger API. They must remain UpperCased.
 */

/// TODO: factor out into external config or axios preferences.
const BASE_URL = 'http://localhost:8090';

const client = axios.create({ // all axios can be used, shown in axios documentation
    // TODO: do we really need it?
    // baseURL: BASE_URL,
    baseURL: '/',
    responseType: 'json',
});

// TODO: delay for demonstration's purposes only
const delay = ms => new Promise(res => setTimeout(res, ms));

/****************** Managing pages ******************/

export function GetSectionDetails(id: number) {
    return async (dispatch: Dispatch) => delay(700).then(() => {
        client.get(`/sections/section/${id}`)
            .then(({ data }) => dispatch(creators.setSection(data)))
            .catch(reason => {
                console.log(reason);
                dispatch(creators.loadFailed(reason));
            });
    });
}

export function SetSectionDetails(id: number, type: 'string', value: 'string') {
    // type: String field for editing: name, main_description и т.д.
    // value: New value of needed field
    return async (dispatch: Dispatch) => delay(700).then(() => {
        client.post(`/sections/section/${id}`, { type, value })
            .then(({ data }) => dispatch(creators.setSection(data)))
            .catch(reason => {
                console.log(reason);
                dispatch(creators.loadFailed(reason));
            });
    });
}

/****************** Loading ******************/

export function GetAllSection() {
    return (dispatch: Dispatch) =>
        delay(1000).then(() =>
            client.get('/sections/all')
                .then(({ data }) => dispatch(creators.loadAll(data)))
                .catch(reason => dispatch(creators.loadFailed(reason)))
        );
}

export function GetOrderedByTypeSection() {
    return (dispatch: Dispatch) =>
        delay(2000).then(() =>
            client.get('/sections/byType')
                .then(({ data }) => dispatch(creators.loadByType(data)))
                .catch(reason => dispatch(creators.loadFailed(reason)))
        );
}

/****************** Section ******************/

export function PostNewSection(sectionTypeId: number, sectionName: string) {
    return async (dispatch: Dispatch) => {
        client.post('/sections/section/new', { sectionName, sectionTypeId })
            .then(({ data }) => dispatch(creators.newSection(data)))
            .catch(reason => dispatch(creators.loadFailed(reason)));
    };
}

export function PostEditSection(id: number, newName: string) {
    return async (dispatch: Dispatch) => delay(1000).then(() => {
        client.post(`/sections/section/${id}/edit`, { name: newName })
            .then(({ data }) => dispatch(creators.editSection(data)))
            .catch(reason => dispatch(creators.loadFailed(reason)));
    });
}

export function PostDeleteSection(section: Section) {
    const { id } = section;
    return async (dispatch: Dispatch) => delay(1000).then(() => {
        client.post(`/sections/section/${id}/delete`)
            .then(() => dispatch(creators.deleteSection(section)))
            .catch(reason => dispatch(creators.loadFailed(reason)));
    });
}

/****************** SectionType ******************/

export function PostNewSectionType(sectionTypeName: string) {
    return async (dispatch: Dispatch) => {
        client.post('/sections/section_type/new', { sectionTypeName })
            .then(({ data }) => dispatch(creators.newSectionType(data)))
            .catch(reason => dispatch(creators.loadFailed(reason)));
    };
}

export function PostEditSectionType(id: number, newName: string) {
    return async (dispatch: Dispatch) => delay(1000).then(() => {
        client.post(`/sections/section_type/${id}/edit`, { name: newName })
            .then(({ data }) => dispatch(creators.editSectionType(data)))
            .catch(reason => dispatch(creators.loadFailed(reason)));
    });
}

export function PostDeleteSectionType(sectionType: SectionType) {
    const { id } = sectionType;
    return async (dispatch: Dispatch) => delay(1000).then(() => {
        client.post(`/sections/section_type/${id}/delete`)
            .then(() => dispatch(creators.deleteSectionType(sectionType)))
            .catch(reason => dispatch(creators.loadFailed(reason)));
    });
}
