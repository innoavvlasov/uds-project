import moxios from 'moxios';
import React from 'react';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import {mount, configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as Thunk from 'redux-thunk';
import renderer from 'react-test-renderer';

configure({adapter: new Adapter()});

import App from '../app';
import MainPage from '../containers/MainPage/index';

let getStore = () => {
    return createStore(combineReducers({app: App['reducer']}), applyMiddleware(Thunk.default));
};

window.alert = (any) => {};

describe('main moxios', () => {
    /*
beforeEach(() => {
    moxios.install()
    let sectionsData = require('../../stub/sections/data/sections.js');
    moxios.stubRequest('/all', {
        status: 200,
        response: sectionsData
            .flatMap(section => section.sections)
            .sort((a, b) => a.name.localeCompare(b.name))
    });
    moxios.stubRequest('/byType', {
        status: 200,
        response: sectionsData
    });
    moxios.stubRequest('/sections/all', {
        status: 200,
        response: sectionsData
            .flatMap(section => section.sections)
            .sort((a, b) => a.name.localeCompare(b.name))
    });
    moxios.stubRequest('/sections/byType', {
        status: 200,
        response: sectionsData
    });
    // я отчаялся и решил вообще все запросы стабать :)))
    moxios.stubRequest('.*', {
        status: 200,
        response: []
    });

});

afterEach(() => moxios.uninstall());
     */

    it('draws 1 page', () => {

        const element = mount(
            <Provider store={getStore()}>
                <App/>
            </Provider>
        );
        expect(element.html()).toMatchSnapshot();
    })
});
