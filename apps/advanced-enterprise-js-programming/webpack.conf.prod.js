const path = require("path");
const webpack = require('webpack');

const outDir = 'dist';
const buildPath = path.resolve(__dirname, outDir);
module.exports = {
    entry: {
        index: ["./src/app.tsx"]
    },
    mode: "production",
    output: {
        filename: 'index.js',
        path: buildPath,
        globalObject: `(typeof self !== 'undefined' ? self : this)`,
        libraryTarget: 'umd',
        chunkFilename: 'sectionsApp.js',
        publicPath: "/uds-sections-static/"
    },
    node: {
        fs: 'empty'
    },
    plugins: [
        new webpack.DefinePlugin({
            'typeof window': JSON.stringify('object')
        })
    ],
    devtool: "none",
    resolve: {
        modules: ['node_modules', 'src'],
        extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.css', '.scss']
    },
    module: {
        rules: [
            {parser: {system: false}},
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader'
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {loader: "css-modules-typescript-loader"},
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: "[name]_[local]_[hash:base64:5]",
                            },
                            importLoaders: 1,
                            sourceMap: true
                        }
                    },
                    {loader: 'resolve-url-loader'},
                    {loader: 'sass-loader'}
                ]
            },
            {
                test: /\.(jpg|png|gif|svg|pdf|ico|ttf|eot|woff|woff2)$/,
                use: [
                    {
                        loader: 'file-loader',
                    }
                ]
            },
            {
                test: /\.js$/,
                use: ["source-map-loader"],
                enforce: "pre"
            },
        ]
    },
    externals: {
        'react': 'react',
        'react-dom': 'react-dom',
        'redux': 'redux',
        'react-redux': 'react-redux',
        'styled-components': 'styled-components'
    }
};
