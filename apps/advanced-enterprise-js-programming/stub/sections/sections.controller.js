const express = require('express');
const sectionsData = require('./data/sections');
const footballSection = require('./data/football_section');

const router = express.Router();

const isServerWorks = true;

/****************** Managing pages ******************/

/** GetSectionDetails */
router.get('/section/:id(\\d+)', async function GetSectionDetails(req, res, next) {
    try {
        return footballSection ?
            res.json(footballSection) :
            res.status(404).json({ message: 'Секция отсутствует' });
    } catch (error) {
        console.log(error);
        next(error);
    }
});

router.post('/section/:id(\\d+)', async function SetNewSectionInformation(req, res, next) {
    try {
        const { type, value } = req.body;
        let section = footballSection;
        switch (type) {
            case 'name':
                section.name = value;
                break;
            case 'main_description':
                section.main_description = value;
                break;
            case 'description_title':
                section.description_title = value;
                break;
            case 'description_body':
                section.description_body = value;
                break;
            default:
                break;
        }
        return section ?
            res.json(section) :
            res.status(404).json({ message: 'Секция отсутствует' });
    } catch (error) {
        console.log(error);
        next(error);
    }
});

/****************** Loading ******************/

/** GetAllSection */
router.get('/all', async function GetAllSection(req, res, next) {
    try {
        let response = sectionsData
            .flatMap(section => section.sections)
            .sort((a, b) => a.name.localeCompare(b.name));
        return response ?
            res.json(response) :
            res.status(404).json({ message: 'Секции отсутствуют' });
    } catch (error) {
        console.log(error);
        next(error);
    }
});


/** GetOrderedByTypeSection */
router.get('/byType', async function GetOrderedByTypeSection(req, res, next) {
    try {
        return sectionsData ?
            res.json(sectionsData) :
            res.status(404).json({ message: 'Секции отсутствуют' });
    } catch (error) {
        console.log(error);
        next(error);
    }
});

/****************** Section ******************/

/** PostNewSection */
router.post('/section/new', async function PostNewSection(req, res, next) {
    try {
        const { sectionTypeId, sectionName } = req.body;

        console.log(`adding section with name "${sectionName}" to section type ${sectionTypeId}`);
        const response = {
            section: {
                id: 1000,
                name: sectionName,
            },
            sectionTypeId,
            sectionTypeIndex: 0,
            sectionAllIndex: 0,
        };
        return isServerWorks ?
            res.status(200).json(response) :
            res.status(500).json({});
    } catch (error) {
        console.log(error);
        next(error);
    }
});


/** PostEditSection */
router.post('/section/:id(\\d+)/edit', async function getSections(req, res, next) {
    try {
        const id = Number.parseInt(req.params.id, 10);
        const { name } = req.body;
        const sectionTypeIndex = sectionsData.findIndex(st => ~st.sections.findIndex(s => s.id === id));
        const sectionType = sectionsData[sectionTypeIndex];
        const sectionTypeId = sectionType.id;

        console.log(`editing section with ${id} to "${name}"`);
        const response = {
            section: { id, name },
            sectionTypeId,
            sectionTypeIndex: 0,
            sectionAllIndex: 0,
        };
        return isServerWorks ?
            res.status(200).json(response) :
            res.status(500).json({});
    } catch (error) {
        console.log(error);
        next(error);
    }
});

/** PostDeleteSection */
router.post('/section/:id(\\d+)/delete', async function PostDeleteSection(req, res, next) {
    try {
        const id = Number.parseInt(req.params.id);

        console.log(`delete section with id "${id}"`);
        return isServerWorks ?
            res.status(200).json({}) :
            res.status(500).json({});
    } catch (error) {
        console.log(error);
        next(error);
    }
});

/****************** SectionType ******************/

/** PostNewSectionType */
router.post('/section_type/new', async function PostNewSectionType(req, res, next) {
    try {
        const { sectionTypeName } = req.body;

        console.log(`adding section type with name "${sectionTypeName}"`);

        const response = {
            sectionType: {
                id: 123,
                name: sectionTypeName,
                sections: []
            },
            sectionTypeIndex: 0
        };
        return isServerWorks ?
            res.status(200).json(response) :
            res.status(500).json({});
    } catch (error) {
        console.log(error);
        next(error);
    }
});

/** PostEditSectionType */
router.post('/section_type/:id(\\d+)/edit', async function PostEditSectionType(req, res, next) {
    try {
        const id = Number.parseInt(req.params.id);
        const { name } = req.body;

        console.log(`editing section type with ${id} to "${name}"`);
        const response = {
            sectionType: {
                id,
                name,
                sections: []
            },
            sectionTypeIndex: 0
        };
        return isServerWorks ?
            res.status(200).json(response) :
            res.status(500).json({});
    } catch (error) {
        console.log(error);
        next(error);
    }
});

/** PostDeleteSectionType */
router.post('/section_type/:id(\\d+)/delete', async function PostDeleteSectionType(req, res, next) {
    try {
        const id = Number.parseInt(req.params.id);

        console.log(`delete section type with id "${id}"`);
        return isServerWorks ?
            res.status(200).json({}) :
            res.status(500).json({});
    } catch (error) {
        console.log(error);
        next(error);
    }
});

module.exports = router;
