module.exports = {
    "id": 1,

    "name": "Мини-футбол",

    "main_description": "Открыта запись в новые клубы и студии ГБУ \"Ратмир\"! Бесплатный тренажерный зал для взрослых и секции боевых искусств для детей от 4 лет.",

    "main_pic_url": "http://delabuga.ru/wp-content/uploads/2018/03/2472554.jpg",

    "description_title": "Что такое мини-футбол и в чём секрет его популярности",

    "description_body": "Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные? Непонятно как такое может быть? Никакого обмана! \
    Давайте знакомиться, посмотрите видео и если останутся вопросы, позвоните нам, мы готовы принять ваш звонок 24/7 и ответить на все вопросы. Мы предложим спортивные, \
    творческие и развивающие кружки и секции для детей.",

    "last_picture": "https://yandex.ru/images/search?pos=3&img_url=https%3A%2F%2Fpbs.twimg.com%2Fmedia%2FC-S0ywLXkAAy65o.jpg%3Alarge&text=%D1%87%D0%B8%D1%85%D1%83%D0%B0%D1%85%D1%83%D0%B0%20%D0%BC%D0%B5%D0%BC&rpt=simage",

    "pictures": [
        "https://avatars.mds.yandex.net/get-pdb/1039768/2383260b-b7a8-4b30-97ef-bf80d5a7dbd0/s1200",
        "https://pbs.twimg.com/media/DuSI3r7WoAA3oR0.jpg:large",
        "https://www.mvestnik.ru/mvfoto/2019/04/17/mini-futbol.jpg",
        "https://xn--n1abebi.xn--80aaccp4ajwpkgbl4lpb.xn--p1ai/novosti/Futbol-turnir.jpg",
        "https://pbs.twimg.com/media/DUSYweqW4AE4C7t.jpg:large",
        "http://inivanteevka.ru/upload/page/99/39099_picture_d293c222a318af6d86faaf21fa4abad79d90de89.jpg"

    ],

    "video_url":"https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
};
