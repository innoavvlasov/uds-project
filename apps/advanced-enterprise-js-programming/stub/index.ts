const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const path = require('path');
const proxy = require('express-http-proxy');

const CONTENT_ROOT = path.resolve(__dirname, "..", "dist");
const UPSTREAM_SECTIONS = process.env['UPSTREAM_SECTIONS'] || 'localhost:4000';

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.use(express.static(CONTENT_ROOT));
// app.use('/sections', proxy(UPSTREAM_SECTIONS));
app.use('/sections', require('./sections/sections.controller'));

app.listen(8001, () => console.log("Listening on port 8001!"));

module.exports = app;
