const express = require("express");
const app = express();
const fs = require('fs');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(cors()); // to avoid cors problem (google it if you dont know)

if(process.env.BD) {
    app.use('/orgs',require('./orgs/orgs.controller'));
} else {
		app.use('/orgs',require('./api'));
}

app.listen(8091, () => console.log("uds-orgs-bh listening on port 8091!"));

module.exports = app;
