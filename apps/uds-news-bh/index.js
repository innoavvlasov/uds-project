const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const path = require('path')
const cors = require('cors')
const mode = process.env.NODE_ENV
const envPath = path.resolve(__dirname, 'configs', (mode === 'development' ? '.env.development' : '.env'))
const fileService = require('./lib/file.service')
require('dotenv').config({ path: envPath })

const imagesStaticPath = fileService.getStaticPath()
const imagesStaticFullPath = path.resolve(__dirname, fileService.getStaticPath())

app.use(cors())
app.use('/' + imagesStaticPath, express.static(imagesStaticFullPath))
app.use(express.static(path.resolve(__dirname, '..', process.env.BUILD_PATH)))
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))

app.use('/news', require('./news/news.controller'))

app.listen(process.env.SERVER_PORT, () => console.log(`Listening on http://localhost:${process.env.SERVER_PORT}`))

module.exports = app
