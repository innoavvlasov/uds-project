const moment = require('moment')

function transform (model) {
  // prepare data
  const data = model.attributes

  // prepare image preview
  const image = {
    src: 'news/' + data.img
  }

  // prepare tags
  var tags = []
  if (data.tags) {
    tags = data.tags.map(title => {
      return {
        text: title
      }
    })
  }

  // transformation to resource
  return {
    id: data.id,
    title: data.title,
    text: data.content,
    author: data.author,
    commentsNumber: data.comments_count,
    likesNumber: data.likes_count,
    date: moment(data.published_at).calendar(),
    img: image,
    tags: tags
  }
}

module.exports = {
  transform
}
