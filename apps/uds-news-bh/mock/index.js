const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()
app.use(cors())
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))

const newsController = require('./news.controller')
app.use('/news', newsController)
app.get('/', (res, req) => {
  req.send('sadas')
})

app.listen(8088, () => console.log('Listening on port 8088'))
