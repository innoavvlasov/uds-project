import * as React from 'react';
import renderer from 'react-test-renderer';
import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});


import {AboutOrganization} from "../components/OrgComponents/AdministrationBlock";
import {SectionsBlock} from "../components/OrgComponents/SectionsBlock";

describe('Organization Components', () => {

    it('About Organization Block', () => {
        const about = renderer.create(<AboutOrganization about="asdasd"/>).toJSON();

        expect(about).toMatchSnapshot();

    });

    it('Section Block', () => {
        const section = renderer.create(<SectionsBlock schedule_url="orion/AAYN.pdf"/>).toJSON();

        expect(section).toMatchSnapshot();

    });

});

import routeReducer from "../data/routeReducer";
import organizationsReducer from "../data/organizationsReducer";
import * as actions from "../data/action-types";

describe('test reducer', () => {
    it('should return the initial state of routeReducer', () => {
        expect(routeReducer(undefined, {})).toEqual({"page": "", "title": "Organizations"});
    });

    it('should return the initial state of organizationsReducer', () => {
        expect(organizationsReducer(undefined, {})).toEqual({list: [], current: null, loading: false, error: null});
    });

    it('should handle LOAD_PAGE', () => {
        const loadPageAction = {
            type: actions.LOAD_PAGE
        };
        // it's empty on purpose because it's just starting to fetch posts
        expect(routeReducer({}, loadPageAction)).toEqual({});
    });

    it('should handle LOAD_ORGANIZATIONS', () => {
        const loadOrganizationsAction = {
            type: actions.LOAD_ORGANIZATIONS
        };
        // it's empty on purpose because it's just starting to fetch posts
        expect(routeReducer({}, loadOrganizationsAction)).toEqual({});
    });

    it('should handle LOAD_CURRENT_ORGANIZATION', () => {
        const loadCurrentOrganizationAction = {
            type: actions.LOAD_CURRENT_ORGANIZATION
        };
        // it's empty on purpose because it's just starting to fetch posts
        expect(routeReducer({}, loadCurrentOrganizationAction)).toEqual({});
    });


});
