import '../../styles/OrgComponent.css';
import React, {Component} from 'react';

class SingleSectionBlock extends Component {


    handleClick = () => {
        this.props.onClick(this.props.ind);
    };

    render() {
        let sections = '';
        let classes = 'sections-age section-color';

        if (this.props.isOpened) {
            sections = (
                <div className="sections-container">
                    {this.props.sections.map((section, i) => {
                        return (
                            <div className="single-opened-section" key={i}>
                                <span>{section.name}</span>
                                <button>Записаться</button>
                            </div>
                        )
                    })}
                </div>
            );
            classes += ' section-opened';
        }
        return (
            <div className={classes}>
                <div className='sections-age-1' onClick={this.handleClick}>
                    <svg viewBox="0 0 20 12" className="sections-svg">
                        <path fill="#1E1D20" fill-rule="nonzero"
                              d="M18.257.331a1 1 0 1 1 1.486 1.338l-9 10a1 1 0 0 1-1.486 0l-9-10A1 1 0 0 1 1.743.331L10 9.505 18.257.331z"
                              opacity=".5"></path>
                    </svg>
                    <h3 className='sections-age-title'>{ this.props.name}</h3>
                </div>
                {sections}
            </div>
        );
    }
}


export default SingleSectionBlock;
