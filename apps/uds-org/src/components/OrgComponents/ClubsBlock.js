import React, {Component} from 'react';
import { Slider } from '@uds/uds-ui';
import "../../styles/Clubs.css";

class ClubsBlock extends Component {
    state = {
        active: 0,
        activeImg: 0
    };

    setActiveClub = (index) => {
        this.setState({...this.state, active: index});
    };

    render() {
        const {clubs} = this.props;
        const photos = clubs[this.state.active].photos.map(val => ('/orgs/shared/content/' + val.photo));
        return (
            <section className="clubs">
                <h2 className='clubs-title'>Клубы и студии</h2>
                <div className="clubs-content">
                    <ul className="clubs-list">
                        {clubs.map((club,index) => (
                            <li key={club.id} className={index === this.state.active ? 'active' : ''} onClick={() => {this.setActiveClub(index)}}>{club.address}</li>
                        ))}
                    </ul>
                    <div className="clubs-photos">
                        <Slider array={photos} />
                    </div>
                </div>
            </section>
        );
    }
}


export default ClubsBlock;
