import React from "react";
import MetaTags from 'react-meta-tags';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import getMainData from './__data__/actions/main';
import { createStructuredSelector } from 'reselect';
import { PageLoader } from 'uds-ui'; 

import { socialLinks } from '@main/__data__/constants';
import { dashboard } from '@main/__data__/reducers';

import {
  AreaIcons,
  Competitions,
  GallarySlider,
  SocialSection,
  TopSlider,
  VideoSection
} from './features/exports';
import { getImgArr, getSlides, getVideoSection, getMainDataLoading } from "./__data__/selectors/mainData";

const getFeatures = (param) => window['launcher'] && window['launcher'][param];

const features = getFeatures('main').features || {};

class App extends React.Component<any, any> {
  componentDidMount() {
    this.props.getMainData();
  }

  render() {
    const { imgArr, slides, videoSection, isLoading } = this.props;

    const handleSignClick = (data) => {
      console.log('handleSignClick');
    }

    if (!imgArr || !slides || !videoSection || isLoading) {
      return <PageLoader />;
    }

    // const { imgArr, imgBasePath, slides, videoSection } = data;
    return (
      <React.Fragment>
        <MetaTags>
          <title>Секции для детей</title>
          <meta name="description" content="Запишитесь на различные секции и занятия на нашем портале. Полный список спортивных городских учреждений и досуговых центров для детей и взрослых. Найдите свой центр в шаговой доступности." />
          <meta property="og:title" content="Главная" />
        </MetaTags>
        <React.Suspense fallback={<div>Загрузка...</div>}>
          {features.topSlider && <TopSlider slides={slides} onAction={handleSignClick} />}
          {features.areaIcons && <AreaIcons />}
          {features.videoSection && <VideoSection data={videoSection} />}
          {features.gallarySlider && <GallarySlider array={imgArr} />}
          {features.socialSection && <SocialSection links={socialLinks} />}
          {features.competitions && <Competitions />}
        </React.Suspense>
      </React.Fragment>
    )
  }
}

const mapStateToProps = () => createStructuredSelector({
    imgArr: getImgArr,
    slides: getSlides,
    videoSection: getVideoSection,
    isLoading: getMainDataLoading
})

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  getMainData
}, dispatch);

const connectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

connectedApp['reducer'] = dashboard;
export default connectedApp;