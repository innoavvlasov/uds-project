import styled from 'styled-components';

export const Wrapper = styled.div`
  text-align: left;
`;

interface MayBeRequired {
  required?: boolean;
}

export const PlaceHolder = styled.div`
  font-size: 10px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: rgb(142,142,143);
  text-transform: uppercase;

  &:after {
    color: #d0021b;
    content: ${(props: MayBeRequired) => props.required ? '" *"' : ''};
  }
`;

export const StyledInput = styled.input`
  width: calc(100% - 30px);
  height: 40px;
  border: 1px solid rgb(206, 208, 218);
  border-radius: 4px;
  color: #a6abb2;
  margin-top: 5px;
  padding: 0 15px;
  font-size: 14px;
  background-color: #ffffff;

  &:focus {
    outline: none;
  }
`;
