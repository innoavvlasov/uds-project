import styled from 'styled-components';
import { Swipeable } from 'react-swipeable';

import { device } from '@main/__data__/constants';
import { TransitionState } from './model';

/**
 * @prop {TransitionState} transitionState Состояние перехода.
 * @prop {number} transitionDist Дистанция смещения при преходе.
 */
interface AnimatedElement {
  transitionState: TransitionState;
  transitionDist: number;
}

const calcResponsive = (min, max) =>
  `calc(${min}px + (${max} - ${min}) * ((100vw - 768px) / (1440 - 768)))`;

export const Wrapper = styled.section`
    display: grid;
    grid-template-columns: calc(100% - 11px);
    grid-template-rows: 180px 1fr 50px;
    grid-gap: 24px;
    font-family: Montserrat;
    font-size: 12px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.17;
    letter-spacing: normal;
    color: black;
    max-width: 1440px;
    max-height: 771px;
    align-self: center;
    justify-self: center;
    margin-bottom: 47px;
    padding: 0 20px;

    @media ${device.tablet} {
        padding: 0 30px;
        grid-gap: 0;
        grid-template-columns: minmax(300px, 554px) minmax(384px, 529px);
        grid-template-rows: auto 200px;
    }

    img {
        max-width: 529px;
        max-height: 447px;
        height: 180px;

        @media ${device.tablet} {
            width: ${calcResponsive(384, 529)};
            height: ${calcResponsive(355, 447)};
        }
    }
`;

export const StyledTextSlider = styled.div`
    display: grid;
    grid-template-rows: auto auto auto;
    grid-row-gap: ${calcResponsive(10, 40)};
    padding-top: ${calcResponsive(40, 130)};
    grid-column: 1;
    grid-row: 2;

    @media ${device.tablet} {
        grid-row: 1;
    }

    @media ${device.laptopL} {
        padding-top: 130px;
        grid-row-gap: 40px;
    }
`;

const transition = (transitionName: string): string => `${transitionName} 0.7s cubic-bezier(.6,.18,.86,.69)`;

export const StyledText1 = styled.div`
    font-size: 22px;
    font-weight: 600;
    font-weight: bold;
    font-style: normal;
    font-family: Montserrat;
    font-stretch: normal;
    line-height: 1;
    letter-spacing: normal;
    color: #1e1d20;
    transition: ${transition('transform')}, ${transition('opacity')};
    opacity: ${(props: AnimatedElement) => props.transitionState === TransitionState.IDLE ? 1 : 0};

    @media ${device.tablet} {
        transform: translateY(${(props: AnimatedElement) => props.transitionState === TransitionState.IDLE ? '0' : props.transitionDist + 'px'});
        font-size: ${calcResponsive(32, 68)};
    }

    @media ${device.laptopL} {
        font-size: 68px;
    }
`;

export const StyledText2 = styled.div`
    line-height: 1.5;
    font-size: 16px;
    margin-top: 8px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #1e1d20;
    transition: ${transition('transform')}, ${transition('opacity')};
    transition-delay: .2s;
    opacity: ${(props: AnimatedElement) => props.transitionState === TransitionState.IDLE ? 1 : 0};

    @media ${device.tablet} {
        transform: translateY(${(props: AnimatedElement) => props.transitionState === TransitionState.IDLE ? '0' : props.transitionDist + 'px'});
        margin-top: 0;
        font-size: ${calcResponsive(12, 16)};
    }

  @media ${device.laptopL} {
    font-size: 16px;
  }
`;

export const StyledImgSlider = styled(Swipeable)`
    max-width: 1440px;
    display: flex;
    justify-content: center;
    transition: ${transition('opacity')};
    opacity: ${(props: AnimatedElement) => props.transitionState === TransitionState.IDLE ? 1 : 0};
    grid-column: 1;
    grid-row: 1;

    @media ${device.tablet} {
        grid-column: 2;
        grid-row: 1;
    }
`;

export const StyledButton = styled.button`
    border-radius: 28px;
    background-color: #003eff;
    font-size: 18px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #ffffff;
    padding: 13px 34px;
    outline: none;
    cursor: pointer;
    border: none;
    transition: ${transition('opacity')};
    margin-top: 24px;
    width: 100%;

    @media ${device.tablet} {
        width: auto;
        margin-top: 0;
        opacity: ${(props: AnimatedElement) => props.transitionState === TransitionState.IDLE ? 1 : 0};
    }
`;
