import React from 'react';

import { social } from '@main/assets';
import { SocialTitle, IconstBlock } from './styled';

interface SocialLinks {
    vk: string;
    instagram: string;
    ok: string;
    facebook: string;
    twitter: string;
}

interface SocialProps {
    links: SocialLinks;
}

export const SocialSection = ({ links }: SocialProps) => (
    <section>
        <SocialTitle><span>Мы в социальных сетях</span><span>Мы в соцсетях</span></SocialTitle>
        <IconstBlock>
            <a href={links.vk} target="_blank">
                <img src={social.icons.vk} alt="vkontakte"/>
            </a>
            <a href={links.instagram} target="_blank">
                <img src={social.icons.instagram} alt="instagram"/>
            </a>
            <a href={links.ok} target="_blank">
                <img src={social.icons.ok} alt="odnoklassniki"/>
            </a>
            <a href={links.facebook} target="_blank">
                <img src={social.icons.facebook} alt="facebook"/>
            </a>
            <a href={links.twitter} target="_blank">
                <img src={social.icons.twitter} alt="twitter"/>
            </a>
        </IconstBlock>
    </section>
);
