import * as React from 'react';
const getConfigValue = (param) => window['config'] && window['config'][param];

const resourcesUrl = getConfigValue('resourcesUrl') || '';
console.log('resourcesUrl', resourcesUrl);

export const AreaIcons = React.lazy(() =>
  import(/* webpackChunkName: "area-icons" */ `${resourcesUrl}/uds-main/area-icons.js`)
    .then(({ AreaIcons }) => ({ default: AreaIcons })),
);

export const Competitions = React.lazy(() =>
  import(/* webpackChunkName: "competitions" */ `${resourcesUrl}/uds-main/competitions.js`)
    .then(({ Competitions }) => ({ default: Competitions })),
);

export const GallarySlider = React.lazy(() =>
  import(/* webpackChunkName: "gallary-slider" */ `${resourcesUrl}/uds-main/gallary-slider.js`)
    .then(({ GallarySlider }) => ({ default: GallarySlider })),
);

// export const MapWithForm = React.lazy(() =>
//   import(/* webpackChunkName: "map-with-form" */ './map-with-form')
//     .then(({ MapWithForm }) => ({ default: MapWithForm })),
// );

export const SocialSection = React.lazy(() =>
  import(/* webpackChunkName: "social" */ `${resourcesUrl}/uds-main/social.js`)
    .then(({ SocialSection }) => ({ default: SocialSection })),
);

export const TopSlider = React.lazy(() =>
  import(/* webpackChunkName: "top-slider" */ `${resourcesUrl}/uds-main/top-slider.js`)
    .then(({ TopSlider }) => ({ default: TopSlider })),
);

export const VideoSection = React.lazy(() =>
  import(/* webpackChunkName: "video-section" */ `${resourcesUrl}/uds-main/video-section.js`)
    .then(({ VideoSection }) => ({ default: VideoSection })),
);