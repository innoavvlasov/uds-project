export interface AsyncDataState<T, E = string> {
  loading: boolean;
  data?: T;
  error?: E;
}

// Информация по запросу данных для страницы.
export interface MainDataRequest {
  status: Status;
  mainData: MainData;
}

export interface MainData {
  slides: Slide[];
  imgArr: ImgArr[];
  videoSection: VideoSection;
}

export interface VideoSection {
  title: string;
  videoUrl: string;
  descriptions: string;
}

export interface ImgArr {
  alt: string;
  src: string;
  title: string;
}

export interface Slide {
  alt: string;
  title: string;
  message: string;
  picture: string;
}

interface Status {
  code: number;
}