module.exports = {
  env: {
    browser: false
  },
  parserOptions: {
    project: './tsconfig.json',
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  extends: [
    'eslint:recommended',
    'standard',
  ],
  rules: {
    'indent': ['error', 2],
    'unicode-bom': 'off'
  },
  overrides: [
    {
      "files": ["**/*.js"],
      "rules": {
        "no-unused-vars": "off",
      }
    }
  ]
}
