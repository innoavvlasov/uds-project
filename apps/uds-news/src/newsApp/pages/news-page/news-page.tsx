import React from 'react'
import News from './news/news'

const NewsPage: React.FunctionComponent<any> = ({ setCurNew }) => (
  <News setCurNew={setCurNew}/>
)

export default NewsPage
