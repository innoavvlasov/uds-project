import React from 'react'
import axios from 'axios'
import { TextInput, Button } from 'uds-ui'
import TextButton from 'Components/UI/text-button/text-button'
import Modal from 'Components/UI/modal/modal'

const toBase64 = (file: any) => {
  return new Promise<string>((resolve, reject) => {
    const fileReader = new FileReader()
    fileReader.onloadend = () => resolve(fileReader.result as string)
    fileReader.onerror = () => reject(new Error(''))
    fileReader.readAsDataURL(file)
  })
}
const submitAddNew = async (event: React.SyntheticEvent, updateNews: Function, closeModal: Function) => {
  event.persist()
  event.preventDefault()
  const inputs: any = event.currentTarget.querySelectorAll('input')
  const data: any = {}
  for (const input of inputs) {
    const name = input.name.split('-')[1]
    let value = input.value
    if (name === 'img') {
      try {
        const file = input.files[0]
        const ext = file.name.split('.').slice(-1)[0]
        value = `data:image/${ext};base64,${(await toBase64(input.files[0])).split(',', 2)[1]}`
        console.log(value)
      } catch (error) {
        console.log(error)
        value = null
      }
    }
    if (value) {
      data[input.name.split('-')[1]] = value
    }
  }
  data['comments_count'] = 0
  data['likes_count'] = 0
  axios.post(`${process.env.API_URL}/news/api/news`, data, {
    headers: {
      Authorization: localStorage.getItem('user') && 'Bearer ' + JSON.parse(localStorage.getItem('user')).token
    }
  })
    .then(({ data: news }) => {
      closeModal(false)
      updateNews({ ...news })
    })
}

const AddNew: React.FunctionComponent<any> = ({ updateNews }) => {
  const [isModal, setIsModal] = React.useState(false)
  const [form, setForm] = React.useState<any>({
    title: {
      id: 'addNewsTitle',
      name: 'news-title',
      value: '',
      type: 'text',
      required: true,
      label: 'Заголовок',
      onChange: (value: React.SyntheticEvent<HTMLInputElement>) => {
        setForm((prevForm: any) => (
          { ...prevForm, title: { ...prevForm.title, value } })
        )
      }
    },
    content: {
      id: 'addNewsContent',
      name: 'news-content',
      value: '',
      type: 'textarea',
      required: true,
      label: 'Текст',
      onChange: (value: React.SyntheticEvent<HTMLInputElement>) => {
        setForm((prevForm: any) => (
          { ...prevForm, content: { ...prevForm.content, value } })
        )
      }
    },
    author: {
      id: 'addNewsAuthor',
      name: 'news-author',
      value: '',
      type: 'textarea',
      required: true,
      label: 'Автор',
      onChange: (value: React.SyntheticEvent<HTMLInputElement>) => {
        setForm((prevForm: any) => (
          { ...prevForm, author: { ...prevForm.author, value } })
        )
      }
    },
    image: {
      id: 'addNewsImage',
      name: 'news-img',
      type: 'file',
      required: true,
      label: 'Картинка',
      accept: 'image/*'
      // onClick: () => onFileDialogOpenChange(true),
      // onChange: () => onFileDialogOpenChange(false)
    }
  })

  const addForm = (
    <form onSubmit={(e) => submitAddNew(e, updateNews, setIsModal)}>
      {
        Object.keys(form).map((input) => (
          <TextInput label="" key={form[input].id} {...form[input]}/>
        ))
      }
      <Button type="submit">Сохранить</Button>
    </form>
  )

  return (
    <>
      <TextButton component='button' onClick={() => setIsModal(true)}>Добавить новость</TextButton>
      { isModal && <Modal title='Добавление новости' onClose={() => setIsModal(false)}>{addForm}</Modal> }
    </>
  )
}
export default AddNew
