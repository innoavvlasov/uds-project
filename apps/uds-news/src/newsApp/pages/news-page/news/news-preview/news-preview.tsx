import React from 'react'
import Tag from '../../../../components/UI/tag/tag'
import { Typography, Button } from 'uds-ui'
import classes from './news-preview.scss'
import MetaInfo from '../../../../components/shared/meta-info/meta-info'
import axios from 'axios'

export interface NewsInterface {
  id: string | number;
  img: { src: string; alt?: string };
  tags: { text: string }[];
  title: string;
  author: string;
  date: string;
  commentsNumber: number;
  likesNumber: number;
  deleteNews: Function;
  setCurNew: any;
}

const deleteNewsById: any = (id: number, deleteEvent: Function) => {
  try {
    axios.delete(process.env.API_URL + `/news/api/news/${id}`,
      {
        headers: {
          Authorization: localStorage.getItem('user') && 'Bearer ' + JSON.parse(localStorage.getItem('user')).token
        }
      }).then(() => {
      deleteEvent({ id })
    })
  } catch (error) {
    console.warn('Error while news deleting', error)
  }
}

const NewsPreview: React.FunctionComponent<NewsInterface> = (props: NewsInterface) => {
  const { id, img, tags, title, author, date, commentsNumber, likesNumber, deleteNews, setCurNew } = props
  return (
    <div>
      {
        <Button className={ classes.deleteButton } onClick={ () => window.confirm('Вы действительно хотите удалить новость?') && deleteNewsById(id, deleteNews) }>
          Удалить новость
        </Button>
      }
      <article className={classes.wrapper}>
        <img className={classes.image} src={img.src} alt={img.alt}/>
        <div className={classes.tags}>
          { tags.map((tag, index) => <Tag key={index + ':' + tag.text} text={tag.text}/>) }
        </div>
        <Typography component='h3' className={classes.title} onClick={setCurNew}>{ title }</Typography>
        <MetaInfo data={[author, date, commentsNumber + ' комментариев', '♥ ' + likesNumber]}/>
      </article>
    </div>
  )
}

export default NewsPreview
