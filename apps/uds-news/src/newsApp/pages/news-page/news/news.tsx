import React, { Dispatch, SetStateAction } from 'react'
import axios from 'axios'

import NewsPreview, { NewsInterface } from './news-preview/news-preview'
import AddNew from './add-new/add-new'

// import img from 'Assets/images/s1200.jpg'
// const news: NewsInterface[] = [
//   {
//     id: 1,
//     title: 'Заголовок какой-то новости, возможно очнь интересной',
//     author: 'Виталий Гройсман',
//     commentsNumber: 3,
//     likesNumber: 1,
//     date: '03 сентября 2019',
//     img: { src: img },
//     tags: [{ text: 'Организация' }]
//   },
//   {
//     id: 2,
//     title: 'Заголовок какой-то новости, возможно очнь интересной',
//     author: 'Виталий Гройсман',
//     commentsNumber: 4,
//     likesNumber: 1,
//     date: '03 сентября 2019',
//     img: { src: img },
//     tags: [{ text: 'Организация' }, { text: 'Орг' }]
//   },
//   {
//     id: 3,
//     title: 'Заголовок какой-то новости, возможно очнь интересной',
//     author: 'Виталий Гройсман',
//     commentsNumber: 3,
//     likesNumber: 0,
//     date: '03 сентября 2019',
//     img: { src: img },
//     tags: [{ text: 'Организация' }]
//   }
// ]

const loadNews = async (setNews: Dispatch<SetStateAction<NewsInterface[]>>) => {
  try {
    const token = localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')).token
    const news: NewsInterface[] = (await axios.get(process.env.API_URL + '/news/api/news',
      {
        headers: {
          Authorization: 'Bearer ' + token
        }
      }
    )).data
    setNews(news)
  } catch (error) {
    console.warn('Error while news fetching', error)
  }
}

const News: React.FunctionComponent<any> = ({ setCurNew }) => {
  React.useEffect(() => { loadNews(setNews) }, [])
  const [news, setNews] = React.useState<NewsInterface[]>([])
  return (
    <section>
      <AddNew
        updateNews={
          (data: NewsInterface) => setNews(
            [data, ...news]
          )
        }
      />
      { news.map(current =>
        <NewsPreview
          key={current.id}
          id={current.id}
          img={current.img}
          tags={current.tags}
          title={current.title}
          author={current.author}
          date={current.date}
          commentsNumber={current.commentsNumber}
          likesNumber={current.likesNumber}
          deleteNews={
            ({ id: deleteId }: { id: number }) => setNews(
              news.filter(({ id }) => id !== deleteId)
            )
          }
          setCurNew={() => setCurNew(current)}
        />)}
    </section>
  )
}

export default News
