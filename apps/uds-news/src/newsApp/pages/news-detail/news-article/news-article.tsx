import React from 'react'
import { Typography } from 'uds-ui'
import QuoteArticle from '../quote-article/quote-article'
import classes from './news-article.scss'
import MetaInfo from '../../../components/shared/meta-info/meta-info'
import Tag from '../../../components/UI/tag/tag'
import ShareSocialsBtnGroup from '../share-socials/share-socials'
import LikeCounter from '../like-counter/like-counter'

interface NewsArticleProps {
  id: string | number;
  headerText: string;
  pTexts: Array<string>;
  quoteText: string;
  img: { src: string; alt?: string };
  tags: { text: string }[];
  author: string;
  date: string;
  commentsNumber: number;
  likesNumber: number;
}

class NewsArticle extends React.Component<NewsArticleProps, any> {
  render () {
    const { headerText, pTexts, quoteText, img, author, commentsNumber, likesNumber, date, tags } = this.props
    const paragraphs = pTexts.map((p, i) => <Typography key={i} component={ 'p' }>{p}</Typography>)
    return (
      <article className={ classes.wrapper }>
        <header className={ classes.header }>
          <Typography component={ 'h1' }>
            { headerText }
          </Typography>
        </header>
        <div className={ classes.metaInfo }>
          <MetaInfo data={[author, date, commentsNumber + ' комментариев', '♥ ' + likesNumber]}/>
        </div>
        <img className={classes.image} src={img.src} alt={img.alt}/>
        {paragraphs}
        <QuoteArticle text={quoteText}/>
        <div className={classes.tags}>
          { tags.map((tag, index) => <Tag key={index + ':' + tag.text} text={tag.text}/>) }
        </div>
        <div className={classes.shareSocials}>
          <ShareSocialsBtnGroup/>
        </div>
        <div>
          <LikeCounter likeCount={likesNumber}/>
        </div>
      </article>
    )
  }
}

export default NewsArticle
