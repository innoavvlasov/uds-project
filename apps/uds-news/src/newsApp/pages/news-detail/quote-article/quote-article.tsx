import React from 'react'
import { Typography } from 'uds-ui'
import imageQuote from '../../../../assets/images/quote.jpg'
import classes from './quote-article.scss'

interface QuoteArticleProps {
  text: string;
}

class QuoteArticle extends React.Component<QuoteArticleProps, any> {
  render () {
    return (
      <>
        <div className={classes.quote}>
          <div className={classes.image}>
            <img src={imageQuote} alt={'quote image'}/>
          </div>
          <div className={classes.quoteText}>
            <Typography component={'q'}>
              { this.props.text }
            </Typography>
          </div>
        </div>
      </>
    )
  }
}

export default QuoteArticle
