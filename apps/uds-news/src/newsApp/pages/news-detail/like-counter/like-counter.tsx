import React from 'react'
import classes from './like-counter.scss'
import { Typography } from 'uds-ui'
import likeImg from 'Assets/images/like.png'

interface LikeCounterProps {
  likeCount: number;
}

const LikeCounter: React.FunctionComponent<LikeCounterProps> = (props) => {
  const { likeCount } = props
  return (
    <div className={classes.wrapper}>
      <img src={likeImg} alt={''} className={classes.img}/>
      <Typography component={'p'} className={classes.count}>{likeCount}</Typography>
    </div>
  )
}

export default LikeCounter
