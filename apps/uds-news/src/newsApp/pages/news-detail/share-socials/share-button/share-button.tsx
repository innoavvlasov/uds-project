import React, { CSSProperties } from 'react'
import classes from './share-button.scss'
import { Typography } from 'uds-ui'

interface ShareButtonProps {
  img: { src: string; alt?: string };
  shareNumber: string;
  color: {r: string; g: string; b: string};
}

const ShareButton: React.FunctionComponent<ShareButtonProps> = (props) => {
  const { img, shareNumber, color } = props
  const styleImg: CSSProperties = shareNumber === '' ? { marginLeft: '40%' } : {}
  return (
    <div className={classes.wrapper} style={{ backgroundColor: `rgb(${color.r}, ${color.g}, ${color.b}` }}>
      <img className={ classes.image } style={styleImg} src={ img.src } alt={ img.alt }/>
      <Typography component={'p'} className={ classes.shareCount }>{ shareNumber }</Typography>
    </div>
  )
}

export default ShareButton
