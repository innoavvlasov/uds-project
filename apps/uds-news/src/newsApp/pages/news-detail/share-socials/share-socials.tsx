import React from 'react'
import classes from './share-socials.scss'
import { Typography } from 'uds-ui'
import fbLogo from 'Assets/images/socials/fb.png'
import twitterLogo from 'Assets/images/socials/twitter.png'
import pinterestLogo from 'Assets/images/socials/pinterest.png'
import lnkedInnLogo from 'Assets/images/socials/linkedin.jpg'
import mailLogo from 'Assets/images/socials/mail.jpg'
import ShareButton from './share-button/share-button'

const ShareSocialsBtnGroup: React.FunctionComponent = () => {
  const buttons = [
    <ShareButton key = {1} img={{ src: fbLogo, alt: '' }} shareNumber='9.5K' color={{ r: '56', g: '88', b: '153' }}/>,
    <ShareButton key = {2} img={{ src: twitterLogo, alt: '' }} shareNumber='12.5K' color={{ r: '35', g: '172', b: '225' }}/>,
    <ShareButton key = {3} img={{ src: pinterestLogo, alt: '' }} shareNumber='2K' color={{ r: '203', g: '52', b: '52' }}/>,
    <ShareButton key = {4} img={{ src: lnkedInnLogo, alt: '' }} shareNumber='1.5K' color={{ r: '2', g: '122', b: '182' }}/>,
    <ShareButton key = {5} img={{ src: mailLogo, alt: '' }} shareNumber='' color={{ r: '64', g: '193', b: '250' }}/>
  ]
  return (
    <div className={classes.wrapper}>
      <Typography component={'h3'} className={classes.tag}>
        поделиться
      </Typography>
      <div className={classes.buttons}>
        {buttons.map((element) =>
          <div key={element.key} className={classes.button}>
            {element}
          </div>
        )}
      </div>
    </div>
  )
}

export default ShareSocialsBtnGroup
