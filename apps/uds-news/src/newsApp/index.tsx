import React, { useState } from 'react'
import BaseLayout from './layouts/base-layout/base-layout'
import NewsPage from './pages/news-page/news-page'
import Aside from './components/shared/aside/aside'
import NewsDetailed from './pages/news-detail/news-detail'
import OurSocials from 'Components/shared/socials/our-socials/our-socials'

const NewsApp = () => {
  const [curNew, setCurNew] = useState(null)

  console.log(curNew)
  return (
    <>

      <BaseLayout aside={<Aside/>}>
        { curNew ? <NewsDetailed newsInfo={curNew} resetNewsInfo={() => setCurNew(null)}/> : <NewsPage setCurNew={(newInfo: any) => setCurNew(newInfo)}/> }
      </BaseLayout>
      <OurSocials/>
    </>
  )
}

export default NewsApp
