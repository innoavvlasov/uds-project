import React from 'react'
import cls from './modal.scss'
import { Typography, Button } from 'uds-ui'

export type ModalAction = {
  title: string;
  click: () => void;
}

export interface ModalProps {
  title?: string;
  action?: ModalAction | ModalAction[];
  onClose: () => void;
}

const Modal: React.FunctionComponent<ModalProps> = (props) => {
  React.useEffect(
    () => {
      document.body.style.overflow = 'hidden'
      return () => { document.body.style.overflow = null }
    }
  )
  React.useEffect(
    () => {
      const keyPressHandler = (event: KeyboardEvent) => {
        if (event.key === 'Escape') {
          props.onClose()
        }
      }
      document.addEventListener('keyup', keyPressHandler)
      return () => document.removeEventListener('keyup', keyPressHandler)
    }
  )

  const action: any[] = props.action instanceof Array ? props.action : [props.action]

  return (
    <div className={cls.modalContainer} onClick={props.onClose}>
      <div className={cls.modal} onClick={(event) => event.stopPropagation()}>
        <header>
          <Typography component='h3'>{props.title}</Typography>
          <button className={cls.closeButton} onClick={props.onClose}>X</button>
        </header>
        <main>
          {props.children}
        </main>
        {props.action && (<footer>
          {action.map((action, i) => (
            <Button
              key={i + action.title}
              onClick={action.click}
            >{action.title}</Button>
          ))}
        </footer>
        )}
      </div>
    </div>
  )
}

export default Modal
