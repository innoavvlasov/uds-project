import React from 'react'
import clsx from 'clsx'
import classes from './typography.scss'

interface TypographyProps extends React.HTMLProps<HTMLElement> {
  component?: React.ElementType<React.HTMLAttributes<HTMLElement>>;
  className?: string;
  variant?: TypographyVariantsType;
}

type TypographyVariantsType = 'description'

const variantsMap: Record<TypographyVariantsType, React.ElementType<React.HTMLAttributes<HTMLElement>>> = {
  description: 'span'
}

const Typography = React.forwardRef<HTMLElement, TypographyProps>((props, ref) => {
  const { component, className, variant, ...others } = props

  const Component =
    component ||
    variantsMap[variant] ||
    'span'

  return (
    React.createElement(Component, {
      className: clsx(
        classes.main,
        {
          [classes.h1]: Component === 'h1',
          [classes.h3]: Component === 'h3',
          [classes.p]: Component === 'p',
          [classes.q]: Component === 'q',
          [classes.description]: variant === 'description'
        },
        className
      ),
      ref,
      ...others
    })
  )
})
Typography.displayName = 'Typography'

export default Typography
