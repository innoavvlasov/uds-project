import React from 'react'
import clsx from 'clsx'
import cls from './textarea.scss'
import Typography from 'Components/UI/typography/typography'

type TextareaProps = React.HTMLProps<HTMLTextAreaElement>

const Textarea: React.FunctionComponent<TextareaProps> = (props) => {
  const { id, label, className, rows = 10, ...rest } = props
  return (<div className={clsx(cls.inputWrapper, className)}>
    <Typography component='label' className={cls.label} htmlFor={id}>
      {label}
    </Typography>
    <textarea
      className={clsx(cls.input)}
      rows={rows}
      {...rest}
    />
  </div>)
}

export default Textarea
