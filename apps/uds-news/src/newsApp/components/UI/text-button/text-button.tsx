import React from 'react'
import { Typography } from 'uds-ui'
import cls from './text-button.scss'
import clsx from 'clsx'

const TextButton: React.FunctionComponent<any> = (props) => {
  const { className, variant, ...rest } = props

  return (
    <Typography
      component='button'
      className={clsx(className, cls.textButton, variant === 'secondary' && cls.secondary)}
      {...rest} />
  )
}

export default TextButton
