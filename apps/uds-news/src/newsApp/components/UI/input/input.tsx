import React from 'react'
import clsx from 'clsx'
import cls from './input.scss'
import Typography from 'Components/UI/typography/typography'

type InputProps = React.HTMLProps<HTMLInputElement>

const Input: React.FunctionComponent<InputProps> = (props) => {
  const { id, label, className, type = 'text', ...rest } = props
  return (<div className={clsx(cls.inputWrapper, className)}>
    <Typography component='label' className={cls.label} htmlFor={id}>
      {label}
    </Typography>
    <input
      type={type}
      className={clsx(cls.input)}
      {...rest}
    />
  </div>)
}

export default Input
