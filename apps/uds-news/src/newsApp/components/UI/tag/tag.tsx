import React from 'react'
import { Typography } from 'uds-ui'
import clsx from 'clsx'
import classes from './tag.scss'

export interface TagProps {
  text: string;
  active?: boolean;
  className?: string;
  interactive?: boolean;
}

const Tag: React.FunctionComponent<TagProps> = ({ text, interactive, className, active }) => {
  return (
    <Typography className={clsx(classes.tag, interactive && classes.interactive, active && classes.active, className)}>
      { text }
    </Typography>
  )
}

export default Tag
