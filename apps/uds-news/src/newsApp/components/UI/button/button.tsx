import React from 'react'
import clsx from 'clsx'
import cls from './button.scss'
import Typography from 'Components/UI/typography/typography'

export interface ButtonProps extends React.HTMLAttributes<HTMLButtonElement>{
  variant?: 'primary' | 'secondary';
}

const Button: React.FunctionComponent<ButtonProps> = (props) => {
  const { className, variant, ...rest } = props

  return (
    <Typography component='button' {...(rest)} className={clsx(className, cls.button, variant === 'secondary' && cls.secondary)}/>
  )
}

export default Button
