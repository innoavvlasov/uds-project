import React from 'react'
import Tag, { TagProps } from '../../UI/tag/tag'
import classes from './tags.scss'
import { Typography } from 'uds-ui'

interface TagsProps {
  tags: TagProps[];
}

const Tags: React.FunctionComponent<TagsProps> = ({ tags }) => {
  return (
    <section>
      <Typography component={'h3'} className={classes.title}>Тэги</Typography>
      <div className={classes.tags}>
        { tags.map((tag, index) => (
          <Tag key={index + ':' + tag.text} interactive {...tag} />
        )) }
      </div>
    </section>
  )
}

export default Tags
