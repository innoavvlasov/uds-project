import React from 'react'
import Tags from '../tags/tags'
import Archive from '../archive/archive'

const tags = [
  { text: 'Организации' },
  { text: 'Организации' },
  { text: 'Организации' },
  { text: 'Организации' },
  { text: 'Орг' },
  { text: 'Организации' },
  { text: 'Организации' },
  { text: 'Организации', active: true },
  { text: 'Организации' },
  { text: 'Организации' },
  { text: 'Орг' },
  { text: 'Организации' }
]

const Aside: React.FunctionComponent = () => {
  return (
    <>
      <Tags tags={tags}/>
      <Archive/>
    </>
  )
}

export default Aside
