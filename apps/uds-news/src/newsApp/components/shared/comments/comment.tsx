import React from 'react'
import { Typography } from 'uds-ui'
import styles from './comment.scss'

interface CommentProps {
  userName: string;
  date: string;
  likeCount: number;
  text: string;
  isLiked: boolean;
}

const Comment: React.FunctionComponent<CommentProps> = (props) => {
  const { userName, date, likeCount, text, isLiked } = props
  return (
    <div className={styles['comment-block']}>
      <div className={styles['comment-block_title']}>
        <div className={styles['comment-block_meta']}>
          <Typography component={'span'} className={styles['comment-block_meta-name']}>
            { userName }
          </Typography>
          <Typography component={'span'} className={styles['comment-block_meta-date']}>
            { date }
          </Typography>
        </div>
        <div className={styles['comment-block_heart']}>
          <div className={styles['comment-block_heart-box']}>
            <span className={ isLiked ? styles['comment-block_heart-svg-active'] : styles['comment-block_heart-svg']}/>
          </div>
          <Typography component={'span'} className={styles['comment-block_heart-number']}>
            { likeCount }
          </Typography>
        </div>
      </div>
      <Typography component={'p'} className={styles['comment-block_text']}>
        { text }
      </Typography>
    </div>
  )
}

export default Comment
