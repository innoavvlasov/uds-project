import React from 'react'
import Comment from './comment'
import CommentForm from './comment-form'
import { Typography } from 'uds-ui'

// interface CommentBlockProps {}

const CommentBlock: React.FunctionComponent = () => {
  return (
    <div>
      <Typography component={'h3'}>
        Комментарии
      </Typography>
      <Comment
        userName={ 'Джейсон Стетхес' }
        date={ '29 января 2019' }
        likeCount={ 3 }
        text={ 'Harum incidunt corrupti est velit voluptates reprehenderit esse alias sit dolore, maxime, quaerat quam exercitationem nemo corporis.' }
        isLiked= { false }
      />
      <Comment
        userName={ 'Джон Уик' }
        date={ '29 января 2019' }
        likeCount={ 3 }
        text={ 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.' }
        isLiked= { true }
      />
      <Comment
        userName={ 'Фрэнк Синатра' }
        date={ '29 января 2019' }
        likeCount={ 3 }
        text={ 'Jasse alias sit dolore, maxime, quaerat quam exercitationem nemo corporis.' }
        isLiked= { false }
      />
      <CommentForm />
    </div>
  )
}

export default CommentBlock
