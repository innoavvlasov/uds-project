import React from 'react'
import { TextInput, TextArea, Button } from 'uds-ui'
import styles from './comment.scss'

const CommentForm: React.FunctionComponent = () => {
  return (
    <div className={styles['comment-form']}>
      <TextArea label={ 'Комментарий' } />
      <TextInput label={ 'Имя' } />
      <Button className={styles['comment-form_button']}>Опубликовать</Button>
    </div>
  )
}

export default CommentForm
