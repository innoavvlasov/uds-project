import React from 'react'
import classes from './archive.scss'
import { Typography } from 'uds-ui'

const Archive: React.FunctionComponent = () => {
  return (
    <div className={classes.archive}>
      <Typography component={'h3'}>Архив</Typography>
      <div className={classes.items}>
        <Typography component={'p'} className={classes.item}>
          <a href="https://google.ru">Март 2019</a>
        </Typography>
        <Typography component={'p'} className={classes.item}>
          <a href="https://google.ru">Февраль 2019</a>
        </Typography>
        <Typography component={'p'} className={classes.item}>
          <a href="https://google.ru">Январь 2019</a>
        </Typography>
        <Typography component={'p'} className={classes.item}>
          <a href="https://google.ru">Декабрь 2018</a>
        </Typography>
        <Typography component={'p'} className={classes.item}>
          <a href="https://google.ru">Октябрь 2018</a>
        </Typography>
      </div>
    </div>
  )
}

export default Archive
