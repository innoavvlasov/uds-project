import React from 'react'
import classes from './meta-info.scss'
import { Typography } from 'uds-ui'

interface MetaInfoProps {
  data: string[];
}

const MetaInfo: React.FunctionComponent<MetaInfoProps> = ({ data }) => {
  return (
    <div className={classes.metaWrapper}>
      { data.map((data, index) =>
        <Typography
          key={index + ':' + data}
          variant={'description'}
        >{ data }</Typography>) }
    </div>
  )
}

export default MetaInfo
