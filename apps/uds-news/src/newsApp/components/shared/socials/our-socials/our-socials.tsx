import React from 'react'
import classes from './our-socials.scss'
import { Typography } from 'uds-ui'
import vkIcon from '../../../../../assets/images/socials/vk.png'
import instIcon from '../../../../../assets/images/socials/inst.png'
import twIcon from '../../../../../assets/images/socials/tw.png'
import fbIcon from '../../../../../assets/images/socials/fb.png'
import okIcon from '../../../../../assets/images/socials/ok.png'

const getConfigValue = (param: any) => (window as any)['config'] && (window as any)['config'][param]
const resourcesUrl = getConfigValue('resourcesUrl') || ''
const OurSocials: React.FunctionComponent = () => {
  return (
    <div className={classes.socials}>
      <div className={classes.title}>
        <Typography component={'h3'}>Мы в социальных сетях</Typography>
        <div>
          <a href='https://vk.com'>
            <img src={resourcesUrl + vkIcon} alt='Вконтакте'/>
          </a>
          <a href='https://instagram.com'>
            <img src={resourcesUrl + instIcon} alt='Instagram'/>
          </a>
          <a href='https://ok.ru'>
            <img src={resourcesUrl + okIcon} alt='Одноклассники'/>
          </a>
          <a href='https://fb.com'>
            <img src={resourcesUrl + fbIcon} alt='Facebook'/>
          </a>
          <a href='https://twitter.com'>
            <img src={resourcesUrl + twIcon} alt='Twitter'/>
          </a>
        </div>
      </div>
    </div>
  )
}

export default OurSocials
//
