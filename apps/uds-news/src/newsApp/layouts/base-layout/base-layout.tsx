import React from 'react'
import classes from './base-layout.scss'

interface BaseLayout {
  main?: React.ReactElement;
  aside?: React.ReactElement;
}

const BaseLayout: React.FunctionComponent<BaseLayout> = (props) => {
  return (
    <div className={classes.wrapper}>
      <main className={classes.main}>
        { props.main || props.children }
      </main>
      <aside className={classes.aside}>
        { props.aside }
      </aside>
    </div>
  )
}

export default BaseLayout
