module.exports = {
  env: {
    browser: true
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: './tsconfig.json',
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  settings: {
    react: {
      createClass: "createReactClass", // Regex for ExampleComponent Factory to use,
                                         // default to "createReactClass"
      pragma: "React",  // Pragma to use, default to "React"
      version: "detect",
    }
  },
  plugins: [
    'react',
    '@typescript-eslint'
  ],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'standard',
    'plugin:react/recommended'
  ],
  rules: {
    'indent': 'off',
    'dot-notation': 'off',
    '@typescript-eslint/indent': ['error', 2],
    '@typescript-eslint/explicit-function-return-type': 0,
    '@typescript-eslint/no-use-before-define': ['error', { 'variables': false }]
  },
  overrides: [
    {
      "files": ["**/*.tsx"],
      "rules": {
        "react/prop-types": "off",
        "no-unused-vars": "off",
        "@typescript-eslint/no-explicit-any": "off"
      }
    }
  ]
}
