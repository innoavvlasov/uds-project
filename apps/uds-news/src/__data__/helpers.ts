export function isAdmin (): boolean {
  const user = JSON.parse(localStorage.getItem('user'))
  return user && user.role === 'Admin'
}
