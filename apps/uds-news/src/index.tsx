import React from 'react'
import ReactDOM from 'react-dom'
import NewsApp from './newsApp'

localStorage.setItem('user', JSON.stringify({
  id: 1,
  username: 'admin',
  firstName: 'Admin',
  lastName: 'User',
  role: 'Admin',
  token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInJvbGUiOiJBZG1pbiIsImlhdCI6MTU2OTY5NDIxMn0.gfu4_kBoyBc7H2LU8QjKXL2BqJfiFFpOQsf5uCgLXio'
}))

ReactDOM.render(
  <NewsApp/>,
  document.getElementById('app')
)
