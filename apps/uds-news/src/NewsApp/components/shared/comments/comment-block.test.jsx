import renderer from 'react-test-renderer'
import CommentBlock from './comment-block'
import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

describe('Comment-block component', () => {
    it('Отрисовка', () => {
        const commentBlock = renderer.create(
            <CommentBlock />
        ).toJSON();
        expect(commentBlock).toMatchSnapshot();
    });
})

