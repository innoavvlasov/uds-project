import renderer from 'react-test-renderer'
import CommentForm from './comment-form'
import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

describe('Comment-form component', () => {
    it('Отрисовка', () => {
        const commentForm = renderer.create(
            <CommentForm />
        ).toJSON();
        expect(commentForm).toMatchSnapshot();
    });
})

