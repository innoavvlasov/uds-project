import renderer from 'react-test-renderer'
import Comment from './comment'
import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

describe('Comment component', () => {
    it('Отрисовка', () => {
        const comment = renderer.create(
            <Comment userName={'test'} date={'01-01-2019'} isLiked={true} likeCount={2} text={'comment 1'} />
        ).toJSON();
        expect(comment).toMatchSnapshot();
    });
})

