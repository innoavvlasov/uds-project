import renderer from 'react-test-renderer'
import MetaInfo from './meta-info'
import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

describe('Comment component', () => {
    it('Отрисовка', () => {
        const metainfo = renderer.create(
            <MetaInfo data={['data 1', 'data 2']}/>
        ).toJSON();
        expect(metainfo).toMatchSnapshot();
    });
})

