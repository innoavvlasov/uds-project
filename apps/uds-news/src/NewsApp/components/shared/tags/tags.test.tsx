import renderer from 'react-test-renderer'
import Tags from './tags'
import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })

describe('Tags component', () => {
  it('Tag test', () => {
    const tags = [
      { text: 'Super tag', active: true }
    ]
    const typography = renderer.create(<Tags tags={tags}/>).toJSON()
    expect(typography).toMatchSnapshot()
  })
})
