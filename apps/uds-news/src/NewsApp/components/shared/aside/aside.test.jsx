import renderer from 'react-test-renderer'
import Aside from './aside'
import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

const tags = [
    { text: 'Орг' },
    { text: 'Организации' },
    { text: 'Организации', active: true }
]

describe('Aside component', () => {
    it('Отрисовка', () => {
        const aside = renderer.create(
            <Aside tags={tags}
            />
        ).toJSON();
        expect(aside).toMatchSnapshot();
    });
})

