import renderer from 'react-test-renderer'
import OurSocials from './our-socials'
import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

describe('Comment component', () => {
    it('Отрисовка', () => {
        const ourSocials = renderer.create(
            <OurSocials />
        ).toJSON();
        expect(ourSocials).toMatchSnapshot();
    });
})

