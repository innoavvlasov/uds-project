import renderer from 'react-test-renderer'
import TextButton from './text-button'
import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })

describe('Text button component', () => {
    it('Text button test', () => {
        const textbutton = renderer.create(<TextButton text={'Test button'}/>).toJSON()
        expect(textbutton).toMatchSnapshot()
    })
})
