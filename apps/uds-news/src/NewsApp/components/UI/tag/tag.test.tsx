import renderer from 'react-test-renderer'
import Tag from './tag'
import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })

describe('Tag component', () => {
    it('Tag test', () => {
        const typography = renderer.create(<Tag text={'asd'}/>).toJSON()
        expect(typography).toMatchSnapshot()
    })
})
