const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ModuleReplaceWebpackPlugin = require('module-replace-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const Dotenv = require('dotenv-webpack')
const envPath = path.resolve(__dirname, '.env.development')
require('dotenv').config({ path: envPath })

const outputDirectory = path.resolve(__dirname, '..', process.env.BUILD_PATH)

module.exports = {
  entry: {
    index: ['./src/index.tsx']
  },
  output: {
    path: outputDirectory,
    filename: '[name].js',
    publicPath: '/'
  },
  mode: 'development',
  devtool: 'source-map',
  devServer: {
    contentBase: outputDirectory,
    overlay: true,
    port: 8089,
    public: 'localhost:8089',
    host: '0.0.0.0',
    // hot: true,
    historyApiFallback: true
  },
  module: {
    rules: [
      { enforce: 'pre', test: /\.js$/, loader: 'source-map-loader' },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              configFile: path.resolve(__dirname, '../tsconfig.json')
            }
          },
          {
            loader: 'eslint-loader',
            options: {
              configFile: path.resolve(__dirname, '../src/.eslintrc.js')
            }
          }
        ]
      },
      {
        test: /\.(jpg|png|gif|svg|pdf|ico|ttf|eot|woff|woff2)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              context: 'src'
            }
          }
        ]
      },
      {
        test: /\.(scss|css)$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[name]_[local]_[hash:base64:5]'
              },
              importLoaders: 2,
              sourceMap: true
            }
          },
          { loader: 'resolve-url-loader' },
          { loader: 'sass-loader' }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
    alias: {
      Assets: path.resolve(__dirname, '../src/assets'),
      Components: path.resolve(__dirname, '../src/newsApp/components'),
      Pages: path.resolve(__dirname, '../src/newsApp/pages')
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    new CleanWebpackPlugin(),
    new Dotenv({
      path: envPath
    }),
    new webpack.ProgressPlugin(),
    new ModuleReplaceWebpackPlugin({
      modules: [{
        test: /styled-components/,
        replace: 'styled-components/dist/styled-components.min.js'
      }]
    })
  ]
}
