const path = require('path')
require('dotenv').config({ path: './.env.development' })
const webpack = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const Dotenv = require('dotenv-webpack')
const envPath = path.resolve(__dirname, '.env')
require('dotenv').config({ path: envPath })

const outputDirectory = path.resolve(__dirname, '..', process.env.BUILD_PATH)

module.exports = () => {
  return {
    entry: {
      newsApp: './src/newsApp/index.tsx'
    },
    output: {
      path: outputDirectory,
      filename: '[name].js',
      libraryTarget: 'umd',
      publicPath: '/uds-news/'
    },
    mode: 'production',
    devtool: false,
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'ts-loader',
              options: {
                configFile: path.resolve(__dirname, '../tsconfig.json')
              }
            },
            {
              loader: 'eslint-loader',
              options: {
                configFile: path.resolve(__dirname, '../src/.eslintrc.js')
              }
            }
          ]
        },
        {
          test: /\.(jpg|png|gif|svg|pdf|ico|ttf|eot|woff|woff2)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[path][name].[ext]',
                context: 'src'
              }
            }
          ]
        },
        {
          test: /\.(scss|css)$/,
          use: [
            {
              loader: 'style-loader'
            },
            {
              loader: 'css-loader',
              options: {
                modules: {
                  localIdentName: '[name]_[local]_[hash:base64:5]'
                },
                importLoaders: 2
              }
            },
            // {
            //   loader: 'postcss-loader',
            //   options: {
            //     config: {
            //       path: path.resolve(__dirname, './postcss.config.js')
            //     }
            //   }
            // },
            { loader: 'resolve-url-loader' },
            { loader: 'sass-loader' }
          ]
        }
      ]
    },
    resolve: {
      extensions: ['.js', '.jsx', '.ts', '.tsx'],
      alias: {
        Assets: path.resolve(__dirname, '../src/assets'),
        Components: path.resolve(__dirname, '../src/newsApp/components'),
        Pages: path.resolve(__dirname, '../src/newsApp/pages')
      }
    },
    plugins: [
      new CleanWebpackPlugin(),
      new Dotenv({
        path: envPath
      }),
      new webpack.ProgressPlugin()
    ],
    externals: {
      react: 'react',
      'react-dom': 'react-dom',
      redux: 'redux',
      'react-redux': 'react-redux',
      'styled-components': 'styled-components'
    }
  }
}
