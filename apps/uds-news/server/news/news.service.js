const repository = require('./news.repository')
const fileService = require('../lib/file.service')
const resource = require('./news.resource')

// get all news
async function getNews () {
  const newsList = await repository.getPublishedNews()

  var news = []
  newsList.models.forEach(model => {
    news.push(resource.transform(model))
  })

  return news
}

// find post
async function getPostById (id) {
  const post = await repository.findById(id)
  return resource.transform(post)
}

// create new post from request body
async function createFromRequestBody (body) {
  // prepare tags if exists
  if (body.tags) {
    body.tags = JSON.stringify(body.tags)
  }

  // prepare image if exists
  if (body.img) {
    body.img = fileService.saveFromBase64(body.img)
  }

  // insert data
  return repository.createPost(body)
}

// update post by id from request body
async function updateById (id, body) {
  // try to find post
  const post = await repository.findById(id)

  // prepare tags if exists
  if (body.tags) {
    body.tags = JSON.stringify(body.tags)
  }

  // prepare image if exists
  if (body.img) {
    // delete previous image if exists
    if (post.attributes.img) {
      fileService.deleteFile(post.attributes.img)
    }
    // save new image
    body.img = fileService.saveFromBase64(body.img)
  }
  // update data
  return repository.update(post, body)
}

// delete post
async function deleteById (id) {
  // try to find post
  const post = await repository.findById(id)

  // delete post image if exists
  if (post.attributes.img) {
    fileService.deleteFile(post.attributes.img)
  }

  // delete model
  return repository.deletePost(post)
}

module.exports = {
  getNews,
  createFromRequestBody,
  deleteById,
  updateById,
  getPostById
}
