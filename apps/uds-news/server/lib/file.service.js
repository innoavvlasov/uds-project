const fs = require('fs')
const path = require('path')

function saveFromBase64 (imageBase64) {
  const filename = generateFilename()
  const filepath = path.resolve(__dirname, '..', getStaticPath(), filename)

  fs.writeFile(filepath, imageBase64, { encoding: 'base64' }, function (err) {
    if (err) throw err
  })

  return getStaticPath() + '/' + filename
}

function deleteFile (filepath) {
  const fullpath = path.resolve(__dirname, '..') + '/' + filepath

  fs.unlink(fullpath, (err) => {
    // if (err) throw err
    console.log(err)
  })
}

function generateFilename () {
  return Math.random().toString(36) + '.png'
}

function getStaticPath () {
  return 'static'
}

module.exports = {
  saveFromBase64,
  getStaticPath,
  deleteFile
}
