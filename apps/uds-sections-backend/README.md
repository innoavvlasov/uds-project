# Awesome uds-sections-backend Build with TypeORM

# Configuration

Configuration is done using environment variables.

Sample `.env` file for local development:

```dotenv
UDS_SECRET=secret-sections
API_PORT=4000

TYPEORM_SYNCHRONIZE=true
TYPEORM_LOGGING=false

TYPEORM_CONNECTION=postgres
TYPEORM_HOST=localhost
TYPEORM_PORT=5432
TYPEORM_USERNAME=postgres
TYPEORM_PASSWORD=mysecretpassword
TYPEORM_DATABASE=postgres
```

Sample `.env` file for docker-compose deploy:

```dotenv
UDS_SECRET=secret-sections
API_PORT=4000

TYPEORM_SYNCHRONIZE=true
TYPEORM_LOGGING=false

TYPEORM_CONNECTION=postgres
TYPEORM_HOST=database
TYPEORM_PORT=5432
TYPEORM_USERNAME=postgres
TYPEORM_PASSWORD=mysecretpassword
TYPEORM_DATABASE=postgres
```

# Run

First, configure your local `.env`. If you going to use docker, use second sample from above.

## Run in Docker

In the terminal, navigate to the project directory, then run:
```bash
$ docker-compose build
$ docker-compose up
```

Interrupt (Ctrl-C on linux) to stop it (database will persist).

Re-run `build` step each time server source code changes.

To completely shut down services and drop volumes with the database, execute:
```bash
$ docker-compose down -v
```

## Run on host machine (with externally managed database)

1. Run `npm install` command
2. Setup database settings inside `.env` file (see first sample above)
3. Ensure the database is running

    - Generally we expect the database to be provided and initialized.
    - For **postgres**, use docker-compose service named `database`.
    - For **mysql**, either rewrite docker-compose service named `database` or run the following command in the terminal from the content root of the `uds-project`:
    ```
    docker run --name udsdb -p 3306:3306 -p 33060:33060 \
        -v "$PWD"/apps/uds-news-bh/databaseUP/create_user.sql:/docker-entrypoint-initdb.d/create_user.sql \
        -e MYSQL_ALLOW_EMPTY_PASSWORD=yes \
        -e MYSQL_DATABASE=UDS \
        -d mysql:8 \
        --default-authentication-plugin=mysql_native_password
   ```
4. Run `npm start` command

# API

Endpoints are described in detail in the Swagger 3.0/OpenApi [document](./swagger.yaml) or rendered at the [SwaggerHub](https://app.swaggerhub.com/apis/ratijas/uds-sections/1.0.0).
