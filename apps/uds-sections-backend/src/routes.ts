import {getAllSection} from './controller/GetAllSection';
import {getDetailedSection} from './controller/GetDetailedSection';
import {getOrderedByTypeSection} from './controller/GetOrderedByTypeSection';
import {postDeleteSection} from './controller/PostDeleteSection';
import {postEditSection} from './controller/PostEditSection'
import {postEditSectionType} from './controller/PostEditSectionType';

/**
 * All application routes.
 */
export const AppRoutes = [
    {
        path: "/all",
        method: "get",
        action: getAllSection
    },
    {
        path: "/byType",
        method: "get",
        action: getOrderedByTypeSection
    },
    {
        path: "/section/:id",
        method: "get",
        action: getDetailedSection
    },
    {
        path: "/section_type/:id/edit",
        method: "post",
        action: postEditSectionType
    },
    {
        path: "/section/:id/edit",
        method: "post",
        action: postEditSection
    },
    {
        path: "/section/:id/delete",
        method: "post",
        action: postDeleteSection
    },
];