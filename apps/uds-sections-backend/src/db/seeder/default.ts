import {Connection} from 'typeorm';

import {Section, SectionType} from '../entity';


export const sectionTypes = [
    {id: 1, name: "Боевые искусства"},
    {id: 2, name: "Общее физическое развитие"},
    {id: 3, name: "Теннис"},
    {id: 4, name: "Командные виды спорта"},
    {id: 5, name: "Танцы"},
    {id: 6, name: "Лёгкая атлетика"},
    {id: 7, name: "Другие виды спорта"},
    {id: 8, name: "Различные занятия и секции"},
];

export const sections = [
    {sectionType: 1, name: "Айкидо"},
    {sectionType: 1, name: "Каратэ"},
    {sectionType: 1, name: "Рукопашный бой"},
    {sectionType: 1, name: "Таеквон-до"},
    {sectionType: 1, name: "Самозащита"},
    {sectionType: 1, name: "Самбо"},
    {sectionType: 1, name: "Тайский бокс"},
    {sectionType: 1, name: "Джиу-джитсу"},
    {sectionType: 1, name: "Ушуот"},
    {sectionType: 1, name: "Рукопашный бой и основы фехтования коротким и длинным клинком"},
    {sectionType: 1, name: "Единоборство «Система»"},
    {sectionType: 1, name: "Единоборство «Союз Беркут»"},

    {sectionType: 2, name: "ОФП"},
    {sectionType: 2, name: "Фитнес"},
    {sectionType: 2, name: "Оздоровительная гимнастика"},
    {sectionType: 2, name: "Ритмика"},
    {sectionType: 2, name: "Многофункциональная гимнастика"},
    {sectionType: 2, name: "Тренажерный зал"},
    {sectionType: 2, name: "Китайская гимнастика"},
    {sectionType: 2, name: "Гимнастика для детей"},
    {sectionType: 2, name: "Гиревой спорт"},
    {sectionType: 2, name: "Мас-рестлинг"},

    {sectionType: 3, name: "Большой теннис"},
    {sectionType: 3, name: "Настольный теннис"},

    {sectionType: 4, name: "Хоккей"},
    {sectionType: 4, name: "Баскетбол"},
    {sectionType: 4, name: "Хоккей с шайбой"},
    {sectionType: 4, name: "Футбол"},
    {sectionType: 4, name: "Волейбол"},
    {sectionType: 4, name: "Флорбол"},
    {sectionType: 4, name: "Мини-футбол"},

    {sectionType: 5, name: "Хип-Хоп"},
    {sectionType: 5, name: "Джаз-Фанк"},
    {sectionType: 5, name: "Латино-американские танцы"},
    {sectionType: 5, name: "Спортивно-бальные танцы"},
    {sectionType: 5, name: "Реггитон"},
    {sectionType: 5, name: "Евроденс"},
    {sectionType: 5, name: "Спортивные бальные танцы"},
    {sectionType: 5, name: "Танцы"},
    {sectionType: 5, name: "Хореография"},
    {sectionType: 5, name: "Народные танцы"},
    {sectionType: 5, name: "Восточные танцы"},
    {sectionType: 5, name: "Фламенко"},

    {sectionType: 6, name: "Лёгкая атлетика"},
    {sectionType: 6, name: "Скандинавская ходьба"},
    {sectionType: 6, name: "Атлетическая гимнастика"},

    {sectionType: 7, name: "Шахматы"},
    {sectionType: 7, name: "Городки"},
    {sectionType: 7, name: "Велоспорт"},
    {sectionType: 7, name: "Дартс"},
    {sectionType: 7, name: "Спортивное ориентирование"},

    {sectionType: 8, name: "Творческое развитие"},
    {sectionType: 8, name: "Раннее развитие «Дошколёнок»"},
    {sectionType: 8, name: "Компьютерная грамотность"},
    {sectionType: 8, name: "Занятия для лиц с ограниченными возможностями здоровья (ОВЗ)"},
    {sectionType: 8, name: "Надежды маленький оркестрик"},
    {sectionType: 8, name: "Клуб исторической реконструкции"},
    {sectionType: 8, name: "Рисование"},
    {sectionType: 8, name: "Активное долголетие - Танцы"},
    {sectionType: 8, name: "Активное долголетие – Йога"},
    {sectionType: 8, name: "Раннее физическое развитие с элементами айкидо"},
];

export const details = [
    {
        "name": "Мини-футбол",
        "main_description": "Открыта запись в новые клубы и студии ГБУ \"Ратмир\"! Бесплатный тренажерный зал для взрослых и секции боевых искусств для детей от 4 лет.",
        "main_pic_url": "https://s5o.ru/storage/simple/ru/edt/05/82/63/83/rueb57926644d.jpg",
        "description_title": "Что такое мини-футбол и в чём секрет его популярности",
        "description_body": "Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные? Непонятно как такое может быть? Никакого обмана! Давайте знакомиться, посмотрите видео и если останутся вопросы, позвоните нам, мы готовы принять ваш звонок 24/7 и ответить на все вопросы. Мы предложим спортивные, творческие и развивающие кружки и секции для детей.",
        "last_picture": "http://s015.radikal.ru/i330/1509/e3/02466b7ab91e.jpg",
        "pictures": [
            "https://avatars.mds.yandex.net/get-pdb/1039768/2383260b-b7a8-4b30-97ef-bf80d5a7dbd0/s1200",
            "https://pbs.twimg.com/media/DuSI3r7WoAA3oR0.jpg:large",
            "https://www.mvestnik.ru/mvfoto/2019/04/17/mini-futbol.jpg",
            "https://xn--n1abebi.xn--80aaccp4ajwpkgbl4lpb.xn--p1ai/novosti/Futbol-turnir.jpg",
            "https://pbs.twimg.com/media/DUSYweqW4AE4C7t.jpg:large",
            "http://inivanteevka.ru/upload/page/99/39099_picture_d293c222a318af6d86faaf21fa4abad79d90de89.jpg"

        ],
        "video_url":"https://media.w3.org/2010/05/sintel/trailer_hd.mp4",
    }
];

async function seed(connection: Connection) {
    await connection.createQueryBuilder()
        .delete()
        .from(Section)
        .execute();

    await connection.createQueryBuilder()
        .delete()
        .from(SectionType)
        .execute();

    await connection.createQueryBuilder()
        .insert()
        .into(SectionType)
        .values(sectionTypes)
        .execute();

    await connection.createQueryBuilder()
        .insert()
        .into(Section)
        // @ts-ignore
        .values(sections)
        .execute();

    for (let detail of details) {
        await connection.createQueryBuilder()
            .update(Section)
            .set(detail)
            .where('name = :name', {name: detail.name})
            .execute()
    }
}

export default async (connection: Connection) => {
    const sectionTypeRepository = connection.getRepository(SectionType);
    const sectionTypeCount = await sectionTypeRepository.count();

    if (sectionTypeCount != 0) {
        return;
    }

    await seed(connection);
}