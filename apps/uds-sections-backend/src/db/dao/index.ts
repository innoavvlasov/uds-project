import {SectionDao, SectionDaoTypeOrm} from './SectionDao';
import {SectionTypeDao, SectionTypeDaoTypeOrm} from './SectionTypeDao';


export interface Database {
    getSectionDao(): SectionDao;

    getSectionTypeDao(): SectionTypeDao;
}


export class DatabaseTypeOrm implements Database {
    getSectionDao(): SectionDao {
        return new SectionDaoTypeOrm();
    }

    getSectionTypeDao(): SectionTypeDao {
        return new SectionTypeDaoTypeOrm();
    }
}

const database: Database = new DatabaseTypeOrm();

export default database;
