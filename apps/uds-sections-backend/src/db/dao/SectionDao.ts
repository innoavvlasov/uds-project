import {getManager} from 'typeorm';

import {Section, SectionShort} from '../entity';


export interface SectionDao {
    /**
     * Get full section info
     * @param id Section's ID
     */
    get(id: number): Promise<Section>;

    /**
     * Get list of all sections, ordered alphabetically
     */
    getAll(): Promise<SectionShort[]>;

    /**
     * Update section name
     * @param id Section's ID
     * @param name Section's new name
     */
    updateName(id: number, name: string): Promise<SectionShort>;

    /**
     * Delete section
     * @param id Section's ID
     */
    delete(id: number): Promise<void>;
}


export class SectionDaoTypeOrm implements SectionDao {
    async get(id: number): Promise<Section> {
        return await getManager().getRepository(Section)
            .findOneOrFail(id);
    }

    async getAll(): Promise<SectionShort[]> {
        const repository = getManager().getRepository(SectionShort);

        return await repository.find({
            select: ['id', 'name'],
            order: {name: "ASC"},
        });
    }

    async updateName(id: number, name: string): Promise<SectionShort> {
        const manager = getManager();

        await manager
            .createQueryBuilder()
            .update(SectionShort)
            .set({name})
            .where('id = :id', {id})
            .execute();

        return await manager.getRepository(SectionShort).findOneOrFail(id);
    }

    async delete(id: number): Promise<void> {
        const repository = getManager().getRepository(Section);

        await repository.delete(id);
    }
}