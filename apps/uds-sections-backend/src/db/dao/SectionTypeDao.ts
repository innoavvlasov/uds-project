import {getManager} from 'typeorm';

import {SectionTypeShort} from '../entity';


export interface SectionTypeDao {
    /**
     * Get list of all sections grouped by sections type and ordered alphabetically
     */
    getByType(): Promise<SectionTypeShort[]>;

    /**
     * Update section type name
     * @param id SectionType's ID
     * @param name SectionType's name
     */
    updateName(id: number, name: string): Promise<SectionTypeShort>;
}


export class SectionTypeDaoTypeOrm implements SectionTypeDao {
    async getByType(): Promise<SectionTypeShort[]> {
        return await getManager()
            .getRepository(SectionTypeShort)
            .createQueryBuilder("ST")
            .leftJoinAndSelect("ST.sections", "S")
            .orderBy({
                "ST.name": "ASC",
                "S.name": "ASC",
            })
            .getMany();
    }

    async updateName(id: number, name: string): Promise<SectionTypeShort> {
        const repository = getManager().getRepository(SectionTypeShort);

        await repository
            .createQueryBuilder()
            .update()
            .set({name})
            .where("id = :id", {id})
            .execute();

        return await repository.findOneOrFail(id);
    }
}