import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {Section, SectionShort} from './Section';


@Entity('SectionType')
export class SectionType {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @OneToMany(type => Section, section => section.sectionType)
    sections: Section[];
}

/**
 * Another view on the same `SectionType` entity.
 *
 * This type exists solely for the purpose of typing support, so that
 *  - `SectionType` relates with the `Section` entities, and
 *  - `SectionTypeShort` relates with the `SectionShort` entities.
 */
@Entity('SectionType', {synchronize: false})
export class SectionTypeShort {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @OneToMany(type => SectionShort, section => section.sectionType)
    sections: SectionShort[];
}
