import {Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, Unique} from 'typeorm';

import {SectionType, SectionTypeShort} from './SectionType';


@Entity('Section')
@Unique(['name'])
export class Section {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToOne(type => SectionType, type => type.sections)
    @JoinColumn({name: 'sectionTypeId'})
    sectionType: SectionType;

    @Column("text", {nullable: true})
    main_description: string;

    @Column("text", {nullable: true})
    main_pic_url: string;

    @Column("text", {nullable: true})
    description_title: string;

    @Column("text", {nullable: true})
    description_body: string;

    @Column("text", {nullable: true})
    last_picture: string;

    @Column("simple-json", {nullable: true})
    pictures: string[];

    @Column("text", {nullable: true})
    video_url: string
}

/**
 * Another view on the same `Section` entity.
 *
 * Not a SQL view table due to unnecessary complexity of the latter.
 * Contains only id, name and a reference to the containing SectionType
 * (for `TypeOrm`'s `leftJoinAndSelect` functionality only, by default won't appear on the Section entity itself).
 */
@Entity('Section', {synchronize: false})
@Unique(['name'])
export class SectionShort {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToOne(type => SectionTypeShort, type => type.sections)
    @JoinColumn({name: 'sectionTypeId'})
    sectionType: SectionTypeShort;
}
