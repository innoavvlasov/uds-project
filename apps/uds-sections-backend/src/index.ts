import * as cors from 'cors';
import * as dotenv from 'dotenv';
import * as express from 'express';
import {Request, Response} from 'express';
import 'reflect-metadata';
import {createConnection} from 'typeorm';

import defaultSeed from './db/seeder/default';
import {AppRoutes} from './routes';


dotenv.config();

const API_PORT_KEY = 'API_PORT';
const API_PORT = process.env[API_PORT_KEY] || 4000;

process.env['TYPEORM_ENTITIES'] = 'src/db/entity/**/*.ts';
process.env['TYPEORM_MIGRATIONS'] = 'src/db/subscriber/**/*.ts';

// create connection with database
// note that it's not active database connection
// TypeORM creates connection pools and uses them for your requests
createConnection().then(async connection => {
    await defaultSeed(connection);

    // create express app
    const app = express();

    // useful middleware
    app.use(express.json());
    app.use(cors());

    // register all application routes
    AppRoutes.forEach(route => {
        app[route.method](route.path, (request: Request, response: Response, next: Function) => {
            route.action(request, response)
                .then(() => next)
                .catch(err => next(err));
        });
    });

    // run app
    app.listen(API_PORT, () => console.log(`Listening on port ${API_PORT}!`));

}).catch(error => console.log("TypeORM connection error: ", error));
