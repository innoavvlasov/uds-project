import {Request, Response} from 'express';

import database from '../db/dao';
import {SectionType, SectionTypeShort} from '../db/entity';
import {normalizeName, validator} from './util';

/**
 * Update name of the section type.
 */
export async function postEditSectionType(request: Request, response: Response) {
    // extract data
    const id = Number.parseInt(request.params.id, 10);
    const {name} = request.body;

    // compose entity
    let entity = new SectionTypeShort();
    entity.id = id;
    entity.name = normalizeName(name);

    // validate entity
    if (!validator(entity, response)) {
        return;
    }

    // update entity
    entity = await database.getSectionTypeDao().updateName(id, entity.name);

    response.send(entity);
}
