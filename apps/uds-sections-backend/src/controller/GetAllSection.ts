import {Request, Response} from 'express';

import database from '../db/dao';


export async function getAllSection(request: Request, response: Response) {
    const sections = await database.getSectionDao().getAll();
    response.send(sections);
}