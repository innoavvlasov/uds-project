import {Request, Response} from 'express';

import database from '../db/dao';


export async function getOrderedByTypeSection(request: Request, response: Response) {
    const sections = await database.getSectionTypeDao().getByType();
    response.send(sections);
}