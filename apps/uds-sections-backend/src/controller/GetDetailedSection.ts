import {Request, Response} from 'express';
import {EntityNotFoundError} from 'typeorm/error/EntityNotFoundError';

import database from '../db/dao';
import {getIdFromRequest} from './util';


export async function getDetailedSection(request: Request, response: Response) {
    let id = getIdFromRequest(request);

    if (!Number.isInteger(id)) {
        response.status(400);
        response.send();
        return;
    }

    try {
        let details = await database.getSectionDao().get(id);
        response.send(details);
    } catch (e) {
        if (e instanceof EntityNotFoundError) {
            response.status(404);
            response.send();
        }
    }
}
