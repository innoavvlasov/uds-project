import {Request, Response} from 'express';

import database from '../db/dao';


export async function postDeleteSection(request: Request, response: Response) {
    // extract data
    const id = Number.parseInt(request.params.id, 10);
    if (Number.isNaN(id)) {
        response.status(400);
        return;
    }

    await database.getSectionDao().delete(id);

    response.status(200);
    response.send();
}
