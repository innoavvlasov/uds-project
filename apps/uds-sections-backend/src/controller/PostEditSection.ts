import {Request, Response} from 'express';

import database from '../db/dao';
import {Section, SectionShort} from '../db/entity';
import {normalizeName, validator} from './util';


export async function postEditSection(request: Request, response: Response) {
    // extract data
    const id = Number.parseInt(request.params.id, 10);
    const {name} = request.body;

    // compose entity
    let entity = new SectionShort();
    entity.id = id;
    entity.name = normalizeName(name);

    // validate entity
    if (!validator(entity, response)) {
        return;
    }

    // update entity
    entity = await database.getSectionDao().updateName(id, entity.name);

    response.send(entity);
}
