import {Request, Response} from 'express';
import {Section, SectionShort, SectionType, SectionTypeShort} from '../db/entity';

export function getIdFromRequest(request: Request): number {
    return Number.parseInt(request.params.id, 10)
}

export function validator(entity: SectionShort | SectionTypeShort, response: Response): boolean {
    const result = validate(entity);
    if (!result) {
        response.status(400);
        response.send({ok: false, error: "Validation failed"});
    }
    return result;
}

function validate(it: SectionShort | SectionTypeShort): boolean {
    try {
        return Number.isInteger(it.id) && it.name.length !== 0;
    } catch (e) {
        return false;
    }
}

export function normalizeName(name: string): string {
    return name
        ? capitalize(name.trim().replace(/\s+/gm, ' '))
        : name;
}

export function capitalize(string: string): string {
    return string
        ? string.slice(0, 1).toLocaleUpperCase() + string.slice(1)
        : string;
}
