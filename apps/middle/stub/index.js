const express = require('express');
const proxy = require('http-proxy-middleware');
const {
    applyHbs
} = require('../src');
const config = require('./settings');


const app = express()

applyHbs(app)

app.use(
    '/main',
    proxy({
        target: 'http://localhost:8090'
    })
);

app.use(
    '/uds-main-static',
    proxy({
        target: 'http://localhost:3002'
    })
);

app.use(
    '/news',
    proxy({
        target: 'http://localhost:8088'
    })
);

app.use(
    '/orgs/api',
    proxy({
        target: 'http://localhost:8091'
    })
);

app.use(
    '/orgs/shared',
    proxy({
        target: 'http://localhost:3002'
    })
);

app.use(
    '/sections',
    proxy({
        target: 'http://localhost:8001'
    })
);

app.use(["/"], function (request, response) {

    response.render("index.hbs", {
        resourcesUrl: 'http://localhost:3002',
        ...config
    });
});





app.listen(
    8099,
    console.log(
        '🛠',
        'Dev server for templates available on the port:',
        8099
    )
);
