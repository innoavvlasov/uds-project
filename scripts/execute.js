const { exec } = require('child_process');

const execute = (command) => exec(command, (err, stdout, stderr) => {
    if (err) {
        throw err;
    }

    console.log("\x1b[2m stdout: \x1b[0m", stdout);
    console.log("\x1b[41m stderr: \x1b[0m", stderr);
});

module.exports = execute;
