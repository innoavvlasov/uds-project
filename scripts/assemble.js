const execute = require('./execute');
const fs = require('fs');

const confPath = 'packages/nginx-conf/nginx.conf';

fs.readFile(confPath, 'utf8', function(err, contents) {
    if (err) throw err;

    const dir =  __dirname.replace(/\/scripts/, '');

    let newContent = contents.replace(/set\s\$udsRoot\s"[\w\d\/\<\>\-]*"/gi, `set $udsRoot "${dir}/"`);
    newContent = contents.replace(/error_log\s[\w\d\/\<\>\-\.\;]*/gi, `error_log ${dir}/nginx.log;`);

    console.log('your project directory set to', dir);
    fs.writeFile(confPath, newContent, () => console.log('config updated'))
});

const bootstrapAndUi =  `
    cd packages/uds-bootstrap
    npm run prod
    cd ../uds-ui
    npm run prod
`;

const udsSections =  `
    cd apps/advanced-enterprise-js-programming
    npm run build:prod
`;

const udsMain =  `
    cd apps/uds-main
    npm run prod
`;

const udsLogin =  `
    cd apps/uds-login
    npm run prod
`;

const udsOrgs =  `
    cd apps/uds-org
    npm run build
`;

const udsNews = `
    cd apps/uds-news
    npm run build
`;

execute(bootstrapAndUi);
execute(udsMain);
execute(udsLogin);
execute(udsOrgs);
execute(udsNews);
execute(udsSections);
