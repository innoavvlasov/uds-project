const execute = require('./execute');

const udsMain =  `
cd apps/uds-main-bh
npm start
`;

const udsOrgs =  `
cd apps/uds-orgs-bh
npm start
`;

const udsNews = `
cd apps/uds-news-bh
npm run start:mock
`


const udsSectionsR = `
cd apps/uds-sections-backend
npm start
`;

const udsSections = `
cd apps/advanced-enterprise-js-programming
npm run start:watch
`;

execute(udsOrgs);
execute(udsMain);
execute(udsNews);
execute(udsSections);
